# GENE-DB

**View the live demo at: https://genedb.nog642.com/**

This is a prototype genetic database web app, designed to showcase how genetic
databases can allow for dynamic collaborative editing of metadata.

Existing  genetic databases like [GenBank](https://www.ncbi.nlm.nih.gov/genbank/) 
and [BOLD](https://www.boldsystems.org/) assume that the original metadata
uploaded with the genetic sequence is accurate. But sometimes mistakes are made.
It is possible to edit submissions to these databases, but only the original
uploader can do so. Sometimes the original uploader is not available, or not
aware of an inaccuracy in their submissions, or simply doesn't have time to
manage metadata for their uploaded data.

The collaborative editing in this app was modeled on [iNaturalist](https://www.inaturalist.org/),
which is a database for wildlife observations, rather than genetic data, but has
a comprehensive system for people other than the uploaders to identify
organisms.

This project was for an independent study with [Dr. Jeremy Andersen at the Forest Insect Conservation and Management Laboratory](https://jeremycandersen.com/) at the University of Massachusetts Amherst.

## Instructions to run locally

This project is built using Docker and Docker Compose. The app runs using two Docker
containers.

To run this locally, you need to [install Docker](https://docs.docker.com/engine/install/)
and [install Docker Compose](https://docs.docker.com/compose/install/), then
[clone this repository (with HTTPS)](https://docs.gitlab.com/ee/topics/git/clone.html#clone-with-https).

The app is contained in the `server` directory at the top level of the
repository. To run the server, navigate into the `server` directory using a
terminal, then run `docker compose build`, then run `docker compose up`.

The server should come up and the website should be accessible from a web
browser at http://localhost:6201/.

## Repository overview

The `server` directory contains the code for the app. It contains a
`docker-compose.yml` configuration, and two directories `db` and `webserver`
corresponding to the two Docker containers that the app runs in.

`server/db` is where the database is stored. The database container runs
MongoDB, which stores its data on disk. A docker volume is set up so that this
data gets stored on the host inside `server/db/data` (created when the app is
run for the first time), rather than in the container (which can therefore be
deleted without losing the actual data). There is no code for the database
besides the setup in `docker-compose.yml`, because it is a plain MongoDB
database. There is also a script `server/db/clear_db.sh`, which will delete all
the database data.

`server/webserver` contains the main application. It contains a
`Dockerfile` and a directory `app` which contains the code. The webserver runs
in Flask (a Python webserver framework). Client side static files are in
`server/webserver/app/static` and client side code written in TypeScript is in
`serve/webserver/app/client`.

The top-level `scripts` directory contains various scripts that are not needed
to run the app. In particular, these scripts are used to fill the database with
data downloaded from GenBank and BOLD. A GenBank flatfile parser is included in
these scripts, which if expanded upon could be a useful tool on its own.

## Unfinished features

The following features are unfinished and therefore do not fully work in the
demo:
* The search bar at the top, which was intended to be a keyword-based search,
  does not have any search functionality. Currently it just always returns the
  same result, which is the first 20 items in the database (pagination is also
  not implemented).
* The "Sequence Search" feature is also not implemented. There is a button in
  the navbar that leads to a page with a textbox, but there is no submit button,
  and there is no way to search by sequence in the demo. This would be
  implemented using the BLAST algorithm.
