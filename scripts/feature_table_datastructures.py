#!/usr/bin/env python3
from collections.abc import Callable, Container
from dataclasses import dataclass
from enum import Enum
from functools import cached_property
import re
from typing import Any, ClassVar, Optional, Protocol

from insdc_vocabulary import (DBXRefDatabase, InferenceCategory, InferenceType, MobileElementType,
                              RegulatoryClass, RepeatType, TypeMaterial)
from util import AMINO_ACID_ONE_LETTER_ABBREVIATIONS


class Location:

    @staticmethod
    def _parse_multiarg_operator_args(operator_name, str_in):
        if not (str_in.startswith(f'{operator_name}(') and str_in[-1] == ')'):
            raise ValueError(f'invalid {operator_name} call: {str_in}')
        args = []
        paren_stack = 0
        arg_buf = ''
        for c in str_in[len(operator_name) + 1:-1]:
            if c == ',' and paren_stack == 0:
                args.append(arg_buf)
                arg_buf = ''
            else:
                if c == '(':
                    paren_stack += 1
                elif c == ')':
                    paren_stack -= 1
                arg_buf += c
        return args

    @classmethod
    def from_str(cls, str_val):

        if str_val.startswith('complement('):
            if str_val[-1] != ')':
                raise ValueError(f'invalid complement call: {str_val}')
            return ComplementLocation(Location.from_str(str_val[len('complement('):-1]))
        if str_val.startswith('join'):
            join_args = cls._parse_multiarg_operator_args('join', str_val)
            return JoinLocation(tuple(map(Location.from_str, join_args)))
        if str_val.startswith('order'):
            order_args = cls._parse_multiarg_operator_args('order', str_val)
            return JoinLocation(tuple(map(Location.from_str, order_args)))

        try:
            base_num = int(str_val)
        except ValueError:
            pass
        else:
            return SingleBase(base_num)

        try:
            return BaseRange.from_str(str_val)
        except ValueError:
            pass

        raise NotImplementedError


@dataclass(frozen=True)
class SingleBase(Location):
    base: int


@dataclass(frozen=True)
class BetweenBases(Location):
    first_base: int

    @property
    def second_base(self) -> int:
        return self.first_base + 1


@dataclass(frozen=True)
class SingleBaseInRange(Location):
    """
    Deprecated for new records. Represents a single base within a range.
    """
    start: int
    end: int


@dataclass(frozen=True)
class BaseRange(Location):
    start: int
    end: int
    start_exact: bool
    end_exact: bool

    regex_pattern: ClassVar[str] = r'(<)?([0-9]+)..(>)?([0-9]+)'

    def __repr__(self):
        start_str = str(self.start)
        if not self.start_exact:
            start_str = '<' + start_str
        end_str = str(self.end)
        if not self.end_exact:
            end_str = '>' + end_str
        return f'BaseRange({start_str}, {end_str})'

    @classmethod
    def from_str(cls, str_val):
        match = re.fullmatch(cls.regex_pattern, str_val)
        if match is None:
            raise ValueError(f'invalid base range: {str_val!r}')
        return cls.from_regex_groups(*match.groups())

    @classmethod
    def from_regex_groups(cls, start_unk, start, end_unk, end):
        return cls(
            start=int(start),
            end=int(end),
            start_exact=not start_unk,
            end_exact=not end_unk
        )


@dataclass(frozen=True)
class ComplementLocation(Location):
    complement_of: Location


@dataclass(frozen=True)
class JoinLocation(Location):
    locations: tuple[Location, ...]


@dataclass(frozen=True)
class OrderLocation(Location):
    locations: tuple[Location, ...]


class FeatureKey(Enum):

    # Difference and change features
    # ==========================================================================

    old_sequence = 'old_sequence'

    variation = 'variation'

    modified_base = 'modified_base'

    misc_difference = 'misc_difference'

    # Transcript features
    # ==========================================================================

    prim_transcript = 'prim_transcript'

    precursor_RNA = 'precursor_RNA'

    mRNA = 'mRNA'

    fiveprime_UTR = "5'UTR"

    threeprime_UTR = "3'UTR"

    # region of genome that codes for portion of spliced mRNA, rRNA and tRNA;
    # may contain 5'UTR, all CDSs and 3' UTR
    exon = 'exon'

    # coding sequence; sequence of nucleotides that corresponds with the
    # sequence of amino acids in a protein (location includes stop codon);
    # feature includes amino acid conceptual translation.
    CDS = 'CDS'

    sig_peptide = 'sig_peptide'

    transit_peptide = 'transit_peptide'

    # mature peptide or protein coding sequence; coding sequence for the mature
    # or final peptide or protein product following post-translational
    # modification; the location does not include the stop codon (unlike the
    # corresponding CDS)
    mat_peptide = 'mat_peptide'

    # a segment of DNA that is transcribed, but removed from within the
    # transcript by splicing together the sequences (exons) on either side of it
    intron = 'intron'

    polyA_site = 'polyA_site'

    ncRNA = 'ncRNA'

    rRNA = 'rRNA'

    tRNA = 'tRNA'

    misc_RNA = 'misc_RNA'

    # Binding features
    # ==========================================================================

    primer_bind = 'primer_bind'

    protein_bind = 'protein_bind'

    # site in nucleic acid which covalently or non-covalently binds another
    # moiety that cannot be described by any other binding key
    misc_binding = 'misc_binding'

    # Repeat features
    # ==========================================================================

    repeat_region = 'repeat_region'

    # Recombination features
    # ==========================================================================

    misc_recomb = 'misc_recomb'

    # Structure features
    # ==========================================================================

    stem_loop = 'stem_loop'

    # displacement loop; a region within mitochondrial DNA in which a short
    # stretch of RNA is paired with one strand of DNA, displacing the original
    # partner DNA strand in this region; also used to describe the displacement
    # of a region of one strand of duplex DNA by a single stranded invader in
    # the reaction catalyzed by RecA protein
    D_loop = 'D-loop'

    misc_structure = 'misc_structure'

    # Unsorted
    # ==========================================================================

    # gap between two components of a genome or transcriptome assembly
    assembly_gap = 'assembly_gap'

    # constant region of immunoglobulin light and heavy chains, and T-cell
    # receptor alpha, beta, and gamma chains; includes one or more exons
    # depending on the particular chain
    C_region = 'C_region'

    # region of biological interest identified as a centromere and which has
    # been experimentally characterized
    centromere = 'centromere'

    # Diversity segment of immunoglobulin heavy chain, and T-cell receptor beta
    # chain
    D_segment = 'D_segment'

    # gap in the sequence
    gap = 'gap'

    # region of biological interest identified as a gene and for which a name
    # has been assigned
    gene = 'gene'

    # intervening DNA; DNA which is eliminated through any of several kinds of
    # recombination
    iDNA = 'iDNA'

    # joining segment of immunoglobulin light and heavy chains, and T-cell
    # receptor alpha, beta, and gamma chains
    J_segment = 'J_segment'

    misc_feature = 'misc_feature'

    mobile_element = 'mobile_element'

    N_region = 'N_region'

    operon = 'operon'

    oriT = 'oriT'

    propeptide = 'propeptide'

    regulatory = 'regulatory'

    rep_origin = 'rep_origin'

    S_region = 'S_region'

    source = 'source'

    STS = 'STS'

    telomere = 'telomere'

    tmRNA = 'tmRNA'

    unsure = 'unsure'

    V_region = 'V_region'

    V_segment = 'V_segment'


class Qualifier(str, Enum):
    allele = 'allele'
    altitude = 'altitude'
    anticodon = 'anticodon'
    artificial_location = 'artificial_location'
    bio_material = 'bio_material'
    bound_moiety = 'bound_moiety'
    cell_line = 'cell_line'
    cell_type = 'cell_type'
    chromosome = 'chromosome'
    circular_RNA = 'circular_RNA'
    citation = 'citation'
    clone = 'clone'
    clone_lib = 'clone_lib'
    codon_start = 'codon_start'
    collected_by = 'collected_by'
    collection_date = 'collection_date'
    compare = 'compare'
    country = 'country'
    cultivar = 'cultivar'
    culture_collection = 'culture_collection'
    db_xref = 'db_xref'
    dev_stage = 'dev_stage'
    direction = 'direction'
    EC_number = 'EC_number'
    ecotype = 'ecotype'
    environmental_sample = 'environmental_sample'
    estimated_length = 'estimated_length'
    exception = 'exception'
    experiment = 'experiment'
    focus = 'focus'
    frequency = 'frequency'
    function = 'function'
    gap_type = 'gap_type'
    gene = 'gene'
    gene_synonym = 'gene_synonym'
    geo_loc_name = 'geo_loc_name'
    germline = 'germline'
    haplogroup = 'haplogroup'
    haplotype = 'haplotype'
    host = 'host'
    identified_by = 'identified_by'
    inference = 'inference'
    isolate = 'isolate'
    isolation_source = 'isolation_source'
    lab_host = 'lab_host'
    lat_lon = 'lat_lon'
    linkage_evidence = 'linkage_evidence'
    locus_tag = 'locus_tag'
    macronuclear = 'macronuclear'
    map = 'map'
    mating_type = 'mating_type'
    metagenome_source = 'metagenome_source'
    mobile_element_type = 'mobile_element_type'
    mod_base = 'mod_base'
    mol_type = 'mol_type'
    ncRNA_class = 'ncRNA_class'
    note = 'note'
    number = 'number'
    old_locus_tag = 'old_locus_tag'
    operon = 'operon'
    organelle = 'organelle'
    organism = 'organism'
    partial = 'partial'
    PCR_conditions = 'PCR_conditions'
    PCR_primers = 'PCR_primers'
    phenotype = 'phenotype'
    plasmid = 'plasmid'
    pop_variant = 'pop_variant'
    product = 'product'
    protein_id = 'protein_id'
    proviral = 'proviral'
    pseudo = 'pseudo'
    pseudogene = 'pseudogene'
    rearranged = 'rearranged'
    recombination_class = 'recombination_class'
    regulatory_class = 'regulatory_class'
    replace = 'replace'
    ribosomal_slippage = 'ribosomal_slippage'
    rpt_family = 'rpt_family'
    rpt_type = 'rpt_type'
    rpt_unit_range = 'rpt_unit_range'
    rpt_unit_seq = 'rpt_unit_seq'
    satellite = 'satellite'
    segment = 'segment'
    serotype = 'serotype'
    serovar = 'serovar'
    sex = 'sex'
    specimen_voucher = 'specimen_voucher'
    standard_name = 'standard_name'
    strain = 'strain'
    sub_clone = 'sub_clone'
    submitter_seqid = 'submitter_seqid'
    sub_species = 'sub_species'
    sub_strain = 'sub_strain'
    tag_peptide = 'tag_peptide'
    tissue_lib = 'tissue_lib'
    tissue_type = 'tissue_type'
    transgenic = 'transgenic'
    translation = 'translation'
    transl_except = 'transl_except'
    transl_table = 'transl_table'
    trans_splicing = 'trans_splicing'
    type_material = 'type_material'
    variety = 'variety'


class QualifierValueFormatType(Enum):
    free_text = 'free_text'
    enum = 'enum'
    regex = 'regex'
    custom = 'custom'


@dataclass(frozen=True)
class QualifierSpec:
    class TransformerCallback(Protocol):
        def __call__(self, *group_vals: str) -> Any: ...

    qualifier: Qualifier
    value_required: bool

    # None indicates no value
    value_format_type: Optional[QualifierValueFormatType]

    # for value_format_type enum
    allowed_values: Optional[Container[str]] = None
    enum_tansformer: Optional[Callable[[str], Any]] = None

    # for value_format_type custom
    custom_transformer: Optional[Callable[[str], Any]] = None

    # for value_format_type regex
    value_format_regex: Optional[str] = None
    regex_group_validators: Optional[tuple[Optional[Callable[[str], Optional[str]]], ...]] = None
    regex_group_transformer: Optional[TransformerCallback] = None

    def __post_init__(self):
        if self.value_format_type is None:
            assert not self.value_required

        if self.value_format_type == QualifierValueFormatType.enum:
            assert self.allowed_values is not None
        else:
            assert self.allowed_values is None
            assert self.enum_tansformer is None

        if self.value_format_type == QualifierValueFormatType.custom:
            assert self.custom_transformer is not None
        else:
            assert self.custom_transformer is None

        if self.value_format_type == QualifierValueFormatType.regex:
            assert self.value_format_regex is not None
            assert self.regex_group_validators is not None
            assert self.regex_group_transformer is not None
        else:
            assert self.value_format_regex is None
            assert self.regex_group_validators is None
            assert self.regex_group_transformer is None

    def parse(self, qualifier_val: str) -> Any:
        assert self.value_format_type is not None
        if self.value_format_type == QualifierValueFormatType.free_text:
            return qualifier_val
        if self.value_format_type == QualifierValueFormatType.enum:
            assert qualifier_val in self.allowed_values
            if self.enum_tansformer is None:
                return qualifier_val
            return self.enum_tansformer(qualifier_val)
        if self.value_format_type == QualifierValueFormatType.custom:
            return self.custom_transformer(qualifier_val)
        assert self.value_format_type == QualifierValueFormatType.regex
        match = re.fullmatch(self.value_format_regex, qualifier_val)
        if match is None:
            raise ValueError(f'invalid {self.qualifier}: {qualifier_val!r}')
        groups = match.groups()
        assert len(groups) == len(self.regex_group_validators)
        for group, group_validator in zip(groups, self.regex_group_validators):
            if group_validator is None:
                continue
            error_message = group_validator(group)
            if error_message is not None:
                raise ValueError(f'invalid {self.qualifier}: {qualifier_val!r}, '
                                 f'error was: {error_message}')
        return self.regex_group_transformer(*groups)


QUALIFIER_SPECS = {
    Qualifier.allele: QualifierSpec(
        qualifier=Qualifier.allele,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.altitude: QualifierSpec(
        qualifier=Qualifier.altitude,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.anticodon: NotImplemented,
    Qualifier.artificial_location: QualifierSpec(
        qualifier=Qualifier.artificial_location,
        value_required=False,
        value_format_type=QualifierValueFormatType.enum,
        allowed_values={'heterogeneous population sequenced',
                        'low-quality sequence region'}
    ),
    Qualifier.bio_material: NotImplemented,
    Qualifier.bound_moiety: QualifierSpec(
        qualifier=Qualifier.bound_moiety,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.cell_line: QualifierSpec(
        qualifier=Qualifier.cell_line,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.cell_type: QualifierSpec(
        qualifier=Qualifier.cell_type,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.chromosome: QualifierSpec(
        qualifier=Qualifier.chromosome,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.circular_RNA: QualifierSpec(
        qualifier=Qualifier.circular_RNA,
        value_required=False,
        value_format_type=None
    ),
    Qualifier.citation: QualifierSpec(
        qualifier=Qualifier.citation,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        value_format_regex=r'\[([0-9]+)\]',
        regex_group_validators=(None,),
        regex_group_transformer=int
    ),
    Qualifier.clone: QualifierSpec(
        qualifier=Qualifier.clone,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.clone_lib: QualifierSpec(
        qualifier=Qualifier.clone_lib,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.codon_start: QualifierSpec(
        qualifier=Qualifier.codon_start,
        value_required=True,
        value_format_type=QualifierValueFormatType.enum,
        allowed_values={'1', '2', '3'},
        enum_tansformer=int
    ),
    Qualifier.collected_by: QualifierSpec(
        qualifier=Qualifier.collected_by,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.collection_date: NotImplemented,
    Qualifier.compare: NotImplemented,
    Qualifier.country: QualifierSpec(
        qualifier=Qualifier.country,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        # TODO: parse country, region, and locality properly
        value_format_regex=rf'(.+)(?::(.+))?(?:, (.+))?',
        regex_group_validators=(None, None, None),
        regex_group_transformer=lambda country, region, locality: (country, region, locality)
    ),
    Qualifier.cultivar: NotImplemented,
    Qualifier.culture_collection: QualifierSpec(
        qualifier=Qualifier.culture_collection,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        # TODO: https://www.insdc.org/submitting-standards/controlled-vocabulary-culturecollection-qualifier/
        value_format_regex=r'((?i:[a-z]+)):(?:(.+):)?(.+)',
        regex_group_validators=(None, None, None),
        regex_group_transformer=lambda *x: x
    ),
    Qualifier.db_xref: QualifierSpec(
        qualifier=Qualifier.db_xref,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        value_format_regex=rf'({DBXRefDatabase.regex_pattern}):(.+)',
        regex_group_validators=(None, None),
        regex_group_transformer=lambda database, identifier: (database, identifier)
    ),
    Qualifier.dev_stage: QualifierSpec(
        qualifier=Qualifier.dev_stage,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.direction: NotImplemented,
    Qualifier.EC_number: QualifierSpec(
        qualifier=Qualifier.EC_number,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.ecotype: NotImplemented,
    Qualifier.environmental_sample: NotImplemented,
    Qualifier.estimated_length: NotImplemented,
    Qualifier.exception: NotImplemented,
    Qualifier.experiment: QualifierSpec(
        qualifier=Qualifier.experiment,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        value_format_regex=r'(?:(COORDINATES|DESCRIPTION):)?(.+)',
        regex_group_validators=(None, None),
        regex_group_transformer=lambda category, text: (category, text)
    ),
    Qualifier.focus: NotImplemented,
    Qualifier.frequency: NotImplemented,
    Qualifier.function: QualifierSpec(
        qualifier=Qualifier.function,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.gap_type: NotImplemented,
    Qualifier.gene: QualifierSpec(
        qualifier=Qualifier.gene,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.gene_synonym: NotImplemented,
    Qualifier.geo_loc_name: NotImplemented,
    Qualifier.germline: NotImplemented,
    Qualifier.haplogroup: NotImplemented,
    Qualifier.haplotype: NotImplemented,
    Qualifier.host: QualifierSpec(
        qualifier=Qualifier.host,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.identified_by: NotImplemented,
    Qualifier.inference: QualifierSpec(
        qualifier=Qualifier.inference,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        value_format_regex=(rf'(?:({InferenceCategory.regex_pattern}):)?'
                            rf'({InferenceType.regex_pattern})( \(same species\))?'
                            # TODO: parse format for EVIDENCE_BASIS
                            rf'(?::(.+))?'),
        regex_group_validators=(None, None, None, None),
        regex_group_transformer=lambda category, inference_type, same_species, evidence: (
            None if category is None else InferenceCategory(category),
            InferenceType(inference_type),
            bool(same_species),
            evidence
        )
    ),
    Qualifier.isolate: QualifierSpec(
        qualifier=Qualifier.isolate,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.isolation_source: QualifierSpec(
        qualifier=Qualifier.isolation_source,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.lab_host: QualifierSpec(
        qualifier=Qualifier.lab_host,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.lat_lon: NotImplemented,
    Qualifier.linkage_evidence: NotImplemented,
    Qualifier.locus_tag: NotImplemented,
    Qualifier.macronuclear: NotImplemented,
    Qualifier.map: QualifierSpec(
        qualifier=Qualifier.map,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.mating_type: NotImplemented,
    Qualifier.metagenome_source: NotImplemented,
    Qualifier.mobile_element_type: QualifierSpec(
        qualifier=Qualifier.mobile_element_type,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        value_format_regex=rf'({MobileElementType.regex_pattern})(?::(.+))?',
        regex_group_validators=(None, None),
        regex_group_transformer=lambda mobile_element_type, mobile_element_name: (
            MobileElementType(mobile_element_type),
            mobile_element_name
        )
    ),
    Qualifier.mod_base: NotImplemented,
    Qualifier.mol_type: QualifierSpec(
        qualifier=Qualifier.mol_type,
        value_required=True,
        value_format_type=QualifierValueFormatType.enum,
        allowed_values={
            'genomic DNA',
            'genomic RNA',
            'mRNA',
            'tRNA',
            'rRNA',
            'other RNA',
            'other DNA',
            'transcribed RNA',
            'viral cRNA',
            'unassigned DNA',
            'unassigned RNA'
        }
    ),
    Qualifier.ncRNA_class: NotImplemented,
    Qualifier.note: QualifierSpec(
        qualifier=Qualifier.note,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.number: QualifierSpec(
        qualifier=Qualifier.number,
        value_required=True,
        # TODO: unquoted text (single token)
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.old_locus_tag: NotImplemented,
    Qualifier.operon: NotImplemented,
    Qualifier.organelle: NotImplemented,
    Qualifier.organism: QualifierSpec(
        qualifier=Qualifier.organism,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.partial: NotImplemented,
    Qualifier.PCR_conditions: NotImplemented,
    Qualifier.PCR_primers: NotImplemented,
    Qualifier.phenotype: NotImplemented,
    Qualifier.plasmid: QualifierSpec(
        qualifier=Qualifier.plasmid,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.pop_variant: NotImplemented,
    Qualifier.product: QualifierSpec(
        qualifier=Qualifier.product,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.protein_id: QualifierSpec(
        qualifier=Qualifier.protein_id,
        value_required=True,
        # TODO: this actually has a specific format maybe, it is not a free text type
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.proviral: NotImplemented,
    Qualifier.pseudo: QualifierSpec(
        qualifier=Qualifier.pseudo,
        value_required=False,
        value_format_type=None
    ),
    Qualifier.pseudogene: NotImplemented,
    Qualifier.rearranged: NotImplemented,
    Qualifier.recombination_class: NotImplemented,
    Qualifier.regulatory_class: QualifierSpec(
        qualifier=Qualifier.regulatory_class,
        value_required=False,
        value_format_type=QualifierValueFormatType.enum,
        allowed_values=RegulatoryClass,
        enum_tansformer=RegulatoryClass
    ),
    Qualifier.replace: QualifierSpec(
        qualifier=Qualifier.replace,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.ribosomal_slippage: QualifierSpec(
        qualifier=Qualifier.ribosomal_slippage,
        value_required=False,
        value_format_type=None
    ),
    Qualifier.rpt_family: NotImplemented,
    Qualifier.rpt_type: QualifierSpec(
        qualifier=Qualifier.rpt_type,
        value_required=True,
        value_format_type=QualifierValueFormatType.enum,
        allowed_values=RepeatType,
        enum_tansformer=RepeatType
    ),
    Qualifier.rpt_unit_range: QualifierSpec(
        qualifier=Qualifier.rpt_unit_range,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        value_format_regex=BaseRange.regex_pattern,
        regex_group_validators=(None, None, None, None),
        regex_group_transformer=BaseRange.from_regex_groups
    ),
    Qualifier.rpt_unit_seq: NotImplemented,
    Qualifier.satellite: NotImplemented,
    Qualifier.segment: NotImplemented,
    Qualifier.serotype: QualifierSpec(
        qualifier=Qualifier.serotype,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.serovar: QualifierSpec(
        qualifier=Qualifier.serovar,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.sex: NotImplemented,
    Qualifier.specimen_voucher: NotImplemented,
    Qualifier.standard_name: QualifierSpec(
        qualifier=Qualifier.standard_name,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.strain: QualifierSpec(
        qualifier=Qualifier.strain,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.sub_clone: NotImplemented,
    Qualifier.submitter_seqid: NotImplemented,
    Qualifier.sub_species: QualifierSpec(
        qualifier=Qualifier.sub_species,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.sub_strain: QualifierSpec(
        qualifier=Qualifier.sub_strain,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.tag_peptide: NotImplemented,
    Qualifier.tissue_lib: NotImplemented,
    Qualifier.tissue_type: QualifierSpec(
        qualifier=Qualifier.tissue_type,
        value_required=True,
        value_format_type=QualifierValueFormatType.free_text
    ),
    Qualifier.transgenic: NotImplemented,
    Qualifier.translation: QualifierSpec(
        qualifier=Qualifier.translation,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        value_format_regex=rf'([{AMINO_ACID_ONE_LETTER_ABBREVIATIONS}]+)',
        regex_group_validators=(None,),
        regex_group_transformer=lambda aa_seq: aa_seq
    ),
    Qualifier.transl_except: QualifierSpec(
        qualifier=Qualifier.transl_except,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        # TODO: parse location and amino acid
        value_format_regex=r'\(pos:(.+),aa:(.+)\)',
        regex_group_validators=(None, None),
        regex_group_transformer=lambda *x: x
    ),
    Qualifier.transl_table: QualifierSpec(
        qualifier=Qualifier.transl_table,
        value_required=True,
        value_format_type=QualifierValueFormatType.enum,
        # https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi?mode=c
        allowed_values=set(map(str, range(1, 34))),
        enum_tansformer=int
    ),
    Qualifier.trans_splicing: NotImplemented,
    Qualifier.type_material: QualifierSpec(
        qualifier=Qualifier.type_material,
        value_required=True,
        value_format_type=QualifierValueFormatType.regex,
        value_format_regex=rf'({TypeMaterial.regex_pattern}) of (.+)',
        regex_group_validators=(None, None),
        regex_group_transformer=lambda type_of_type, organism_name: (TypeMaterial(type_of_type),
                                                                     organism_name)
    ),
    Qualifier.variety: NotImplemented
}
assert all(spec is NotImplemented or spec.qualifier == key for key, spec in QUALIFIER_SPECS.items())


@dataclass(frozen=True)
class FeatureSpec:
    feature_key: FeatureKey
    # definition: str
    mandatory_qualifiers: set[Qualifier]
    optional_qualifiers: set[Qualifier]
    # organism_scope
    # molecule_scope
    # references
    # comment

    def __post_init__(self):
        assert not self.mandatory_qualifiers & self.optional_qualifiers

    @cached_property
    def allowed_qualifiers(self):
        return self.mandatory_qualifiers | self.optional_qualifiers


FEATURE_SPECS: dict[FeatureKey, FeatureSpec] = {
    FeatureKey.old_sequence: NotImplemented,
    FeatureKey.variation: FeatureSpec(
        feature_key=FeatureKey.variation,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.frequency,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.phenotype,
            Qualifier.product,
            Qualifier.replace,
            Qualifier.standard_name
        }
    ),
    FeatureKey.modified_base: NotImplemented,
    FeatureKey.misc_difference: FeatureSpec(
        feature_key=FeatureKey.misc_difference,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.clone,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.phenotype,
            Qualifier.replace,
            Qualifier.standard_name,

            # removed in recent revision
            Qualifier.citation
        }
    ),
    FeatureKey.prim_transcript: NotImplemented,
    FeatureKey.precursor_RNA: NotImplemented,
    FeatureKey.mRNA: NotImplemented,
    FeatureKey.fiveprime_UTR: NotImplemented,
    FeatureKey.threeprime_UTR: NotImplemented,
    FeatureKey.exon: FeatureSpec(
        feature_key=FeatureKey.exon,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.EC_number,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.number,
            Qualifier.old_locus_tag,
            Qualifier.product,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.standard_name,
            Qualifier.trans_splicing
        }
    ),
    FeatureKey.CDS: FeatureSpec(
        feature_key=FeatureKey.CDS,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.artificial_location,
            Qualifier.circular_RNA,
            Qualifier.codon_start,
            Qualifier.db_xref,
            Qualifier.EC_number,
            Qualifier.exception,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.number,
            Qualifier.old_locus_tag,
            Qualifier.operon,
            Qualifier.product,
            Qualifier.protein_id,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.ribosomal_slippage,
            Qualifier.standard_name,
            Qualifier.translation,
            Qualifier.transl_except,
            Qualifier.transl_table,
            Qualifier.trans_splicing,

            # removed in recent revision
            Qualifier.citation
        }
    ),
    FeatureKey.sig_peptide: FeatureSpec(
        feature_key=FeatureKey.sig_peptide,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.product,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.standard_name
        }
    ),
    FeatureKey.transit_peptide: NotImplemented,
    FeatureKey.mat_peptide: FeatureSpec(
        feature_key=FeatureKey.mat_peptide,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.EC_number,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.product,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.standard_name
        }
    ),
    FeatureKey.intron: FeatureSpec(
        feature_key=FeatureKey.intron,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.number,
            Qualifier.old_locus_tag,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.standard_name,
            Qualifier.trans_splicing
        }
    ),
    FeatureKey.polyA_site: NotImplemented,
    FeatureKey.ncRNA: NotImplemented,
    FeatureKey.rRNA: FeatureSpec(
        feature_key=FeatureKey.rRNA,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.operon,
            Qualifier.product,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.standard_name
        }
    ),
    FeatureKey.tRNA: FeatureSpec(
        feature_key=FeatureKey.tRNA,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.anticodon,
            Qualifier.circular_RNA,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.operon,
            Qualifier.product,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.standard_name,
            Qualifier.trans_splicing
        }
    ),
    FeatureKey.misc_RNA: FeatureSpec(
        feature_key=FeatureKey.misc_RNA,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.operon,
            Qualifier.product,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.standard_name,
            Qualifier.trans_splicing
        }
    ),
    FeatureKey.primer_bind: NotImplemented,
    FeatureKey.protein_bind: FeatureSpec(
        feature_key=FeatureKey.protein_bind,
        mandatory_qualifiers={Qualifier.bound_moiety},
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.operon,
            Qualifier.standard_name
        }
    ),
    FeatureKey.misc_binding: NotImplemented,
    FeatureKey.repeat_region: FeatureSpec(
        feature_key=FeatureKey.repeat_region,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.rpt_family,
            Qualifier.rpt_type,
            Qualifier.rpt_unit_range,
            Qualifier.rpt_unit_seq,
            Qualifier.satellite,
            Qualifier.standard_name
        }
    ),
    FeatureKey.misc_recomb: NotImplemented,
    FeatureKey.stem_loop: FeatureSpec(
        feature_key=FeatureKey.stem_loop,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.operon,
            Qualifier.standard_name
        }
    ),
    FeatureKey.D_loop: NotImplemented,
    FeatureKey.misc_structure: NotImplemented,
    FeatureKey.assembly_gap: NotImplemented,
    FeatureKey.C_region: NotImplemented,
    FeatureKey.centromere: NotImplemented,
    FeatureKey.D_segment: NotImplemented,
    FeatureKey.gap: NotImplemented,
    FeatureKey.gene: FeatureSpec(
        feature_key=FeatureKey.gene,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.operon,
            Qualifier.product,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.phenotype,
            Qualifier.standard_name,
            Qualifier.trans_splicing
        }
    ),
    FeatureKey.iDNA: NotImplemented,
    FeatureKey.J_segment: NotImplemented,
    FeatureKey.misc_feature: FeatureSpec(
        feature_key=FeatureKey.misc_feature,
        mandatory_qualifiers=set(),
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.number,
            Qualifier.old_locus_tag,
            Qualifier.phenotype,
            Qualifier.product,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.standard_name
        }
    ),
    FeatureKey.mobile_element: FeatureSpec(
        feature_key=FeatureKey.mobile_element,
        mandatory_qualifiers={Qualifier.mobile_element_type},
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.rpt_family,
            Qualifier.rpt_type,
            Qualifier.standard_name
        }
    ),
    FeatureKey.N_region: NotImplemented,
    FeatureKey.operon: NotImplemented,
    FeatureKey.oriT: NotImplemented,
    FeatureKey.propeptide: NotImplemented,
    FeatureKey.regulatory: FeatureSpec(
        feature_key=FeatureKey.regulatory,
        mandatory_qualifiers={Qualifier.regulatory_class},
        optional_qualifiers={
            Qualifier.allele,
            Qualifier.bound_moiety,
            Qualifier.db_xref,
            Qualifier.experiment,
            Qualifier.function,
            Qualifier.gene,
            Qualifier.gene_synonym,
            Qualifier.inference,
            Qualifier.locus_tag,
            Qualifier.map,
            Qualifier.note,
            Qualifier.old_locus_tag,
            Qualifier.operon,
            Qualifier.phenotype,
            Qualifier.pseudo,
            Qualifier.pseudogene,
            Qualifier.standard_name,

            # removed in recent revision
            Qualifier.citation
        }
    ),
    FeatureKey.rep_origin: NotImplemented,
    FeatureKey.S_region: NotImplemented,
    FeatureKey.source: FeatureSpec(
        feature_key=FeatureKey.source,
        mandatory_qualifiers={Qualifier.organism, Qualifier.mol_type},
        optional_qualifiers={
            Qualifier.altitude,
            Qualifier.bio_material,
            Qualifier.cell_line,
            Qualifier.cell_type,
            Qualifier.chromosome,
            Qualifier.clone,
            Qualifier.clone_lib,
            Qualifier.collected_by,
            Qualifier.collection_date,
            Qualifier.country,
            Qualifier.cultivar,
            Qualifier.culture_collection,
            Qualifier.db_xref,
            Qualifier.dev_stage,
            Qualifier.ecotype,
            Qualifier.environmental_sample,
            Qualifier.focus,
            Qualifier.geo_loc_name,
            Qualifier.germline,
            Qualifier.haplogroup,
            Qualifier.haplotype,
            Qualifier.host,
            Qualifier.identified_by,
            Qualifier.isolate,
            Qualifier.isolation_source,
            Qualifier.lab_host,
            Qualifier.lat_lon,
            Qualifier.macronuclear,
            Qualifier.map,
            Qualifier.mating_type,
            Qualifier.metagenome_source,
            Qualifier.note,
            Qualifier.organelle,
            Qualifier.PCR_primers,
            Qualifier.plasmid,
            Qualifier.pop_variant,
            Qualifier.proviral,
            Qualifier.rearranged,
            Qualifier.segment,
            Qualifier.serotype,
            Qualifier.serovar,
            Qualifier.sex,
            Qualifier.specimen_voucher,
            Qualifier.strain,
            Qualifier.sub_clone,
            Qualifier.submitter_seqid,
            Qualifier.sub_species,
            Qualifier.sub_strain,
            Qualifier.tissue_lib,
            Qualifier.tissue_type,
            Qualifier.transgenic,
            Qualifier.type_material,
            Qualifier.variety
        }
    ),
    FeatureKey.STS: NotImplemented,
    FeatureKey.telomere: NotImplemented,
    FeatureKey.tmRNA: NotImplemented,
    FeatureKey.unsure: NotImplemented,
    FeatureKey.V_region: NotImplemented,
    FeatureKey.V_segment: NotImplemented
}
assert all(spec is NotImplemented or spec.feature_key == key for key, spec in FEATURE_SPECS.items())


@dataclass(frozen=True)
class QualifierRecord:
    qualifier_name: Qualifier
    value: Any

    # TODO: maybe this should be __str__ (same for others below)
    def __repr__(self):
        return f'/{self.qualifier_name.value}={self.value!r}'


@dataclass(frozen=True)
class FeatureTableRecord:
    feature_key: FeatureKey
    location: Any  # TODO
    qualifiers: list[QualifierRecord]

    def __repr__(self):
        self_repr = f'FeatureTableRecord({self.feature_key}, {self.location!r}, [\n'
        for qualifier in self.qualifiers:
            self_repr += f'    {qualifier!r},\n'
        self_repr += '])'
        return self_repr


@dataclass(frozen=True)
class FeatureTable:
    records: list[FeatureTableRecord]

    def __repr__(self):
        self_repr = "FeatureTable(records=[\n"
        for record in self.records:
            self_repr += '\n'.join('    ' + line for line in repr(record).split('\n'))
            self_repr += ',\n'
        self_repr += '])'
        return self_repr

    # TODO: add methods to search/manipulate, etc
