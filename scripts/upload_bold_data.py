#!/usr/bin/env python3
import csv
import requests


def main():
    with open('../data/bold_data.tsv') as f:
        tsv_reader = csv.DictReader(f, delimiter='\t')
        for line in tsv_reader:
            if not line['nucleotides']:
                continue
            response = requests.post('http://localhost:6201/api/upload', json={
                'fasta': f">{line['sequenceID']}\n{line['nucleotides'].strip('-')}",
                'metadata': {
                    'name': line['sequenceID'],
                    'organism': line['species_name'] + '\n' + '; '.join([
                        line['phylum_name'],
                        line['class_name'],
                        line['order_name'],
                        line['family_name'],
                        line['subfamily_name'],
                        line['genus_name']
                    ]),
                    'organism_confidence': 'imported_from_database',
                    'comment': ''  # TODO
                }
            })
            try:
                response.raise_for_status()
            except requests.exceptions.HTTPError:
                print(f"HTTP {response.status_code} for {line['sequenceID']}: {response.text}")
            else:
                print(f"{line['sequenceID']}: {response.text}")


if __name__ == '__main__':
    main()
