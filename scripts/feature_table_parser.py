#!/usr/bin/env python3
"""
https://www.insdc.org/submitting-standards/feature-table/
"""
from collections import defaultdict

from feature_table_datastructures import (FeatureKey, FeatureTable, FeatureTableRecord,
                                          FEATURE_SPECS, Location, Qualifier, QualifierRecord,
                                          QUALIFIER_SPECS)
from util import parse_indented_keyvalue


def parse_feature_table(lines: list[str]) -> FeatureTable:
    assert lines[0][:5] == '     '
    assert lines[0][5] != ' '

    parsed_feature_table_records = []
    for key, val_lines in parse_indented_keyvalue(lines):
        feature_key = FeatureKey(key)
        feature_spec = FEATURE_SPECS[feature_key]

        grouped_lines = [[]]
        for line in val_lines:
            if line.startswith('/'):
                line = line[1:]
                grouped_lines.append([])
            grouped_lines[-1].append(line)

        location_lines, *qualifiers_lines = grouped_lines
        location = Location.from_str(''.join(location_lines))

        qualifiers = defaultdict(list)
        for qualifier_lines in qualifiers_lines:
            qualifier_str = '\n'.join(qualifier_lines)
            if '=' in qualifier_str:
                qualifier_name, qualifier_val_raw = qualifier_str.split('=', 1)
                if qualifier_val_raw.startswith('"'):
                    assert qualifier_val_raw[-1] == '"'
                    # undo quote escaping
                    qualifier_val = qualifier_val_raw[1:-1].replace('""', '"')
                else:
                    qualifier_val = qualifier_val_raw

                # TODO: absolute hack, as the continuation being ' ' or '' is inconsistent;
                #       email genbank for help
                if qualifier_name in {Qualifier.translation}:
                    qualifier_val = qualifier_val.replace('\n', '')
                else:
                    qualifier_val = qualifier_val.replace('\n', ' ')

            else:
                qualifier_name = qualifier_str
                qualifier_val = None
            qualifier = Qualifier(qualifier_name)
            if qualifier not in feature_spec.allowed_qualifiers:
                raise ValueError(f'Feature {feature_key} not allowed to have qualifier {qualifier_name}')
            qualifiers[qualifier].append(qualifier_val)

        for required_qualifier in feature_spec.mandatory_qualifiers:
            if required_qualifier not in qualifiers:
                raise ValueError(f'Feature {feature_key} missing mandatory qualifier {required_qualifier}')

        parsed_qualifiers = []
        for qualifier, qualifier_vals in qualifiers.items():
            qualifier_spec = QUALIFIER_SPECS[qualifier]
            for qualifier_val in qualifier_vals:
                if qualifier_val is None:
                    assert not qualifier_spec.value_required
                if qualifier_spec.value_format_type is None:
                    assert qualifier_val is None
                    parsed_qualifier_val = None
                else:
                    parsed_qualifier_val = qualifier_spec.parse(qualifier_val)
                parsed_qualifiers.append(QualifierRecord(qualifier, parsed_qualifier_val))

        parsed_feature_table_records.append(FeatureTableRecord(
            feature_key=feature_key,
            location=location,
            qualifiers=parsed_qualifiers
        ))

    return FeatureTable(parsed_feature_table_records)
