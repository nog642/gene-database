#!/usr/bin/env python3
from __future__ import annotations

import os
from collections import defaultdict
from dataclasses import dataclass
from datetime import date, datetime
import itertools
import re
from typing import Any, Callable, Optional
import warnings

from feature_table_parser import parse_feature_table
from genbank_datastructures import (GenBankMoleculeType, GenBankOrganismDivision, GenBankTopology,
                                    GenBankReference, GenBankLocus, Sequence)
from util import combine_lines, AttrVal, parse_indented_keyvalue


def split_semicolon_list(list_str: str) -> tuple[str, ...]:
    """
    Parse genbank semicolon delimited list format, ending with a period.
    """
    if len(list_str) == 1:
        assert list_str[0] == '.'
        return ()
    assert list_str[-1] == '.'
    return tuple(map(str.strip, list_str[:-1].split(';')))


class GenBankFlatfile:
    """
    https://ncbi.nlm.nih.gov/genbank/release/258/
    """

    @dataclass(frozen=True)
    class _Field:
        required: bool
        single_record: bool
        body: bool

        # one of:
        # - ('standard', 'one_line_header')
        # - ('standard', 'body_only')
        # - ('method', method name)
        # - ('lambda', lambda)
        post_processor: tuple[str, str | Callable]  # method name

        subfields: Optional[dict[str, GenBankFlatfile._Field]] = None

    _FIELDS = {
        'LOCUS': _Field(
            required=True,
            single_record=True,
            body=False,
            post_processor=('method', '_parse_locus_field')
        ),
        'DEFINITION': _Field(
            required=True,
            single_record=False,
            body=False,
            post_processor=('lambda', lambda field_vals: combine_lines(itertools.chain.from_iterable(field_vals)))
        ),
        'ACCESSION': _Field(
            required=True,
            single_record=False,
            body=False,
            post_processor=('lambda', lambda field_vals: tuple(
                combine_lines(itertools.chain.from_iterable(field_vals)).split()
            ))
        ),
        'VERSION': _Field(
            required=True,
            single_record=True,
            body=False,
            post_processor=('standard', 'one_line_header')
        ),
        'DBLINK': _Field(
            required=False,
            single_record=False,
            body=False,
            post_processor=('method', '_parse_dblink_field')
        ),
        'KEYWORDS': _Field(
            required=False,
            single_record=False,
            body=False,
            post_processor=('lambda', lambda field_vals: split_semicolon_list(
                combine_lines(itertools.chain.from_iterable(field_vals))
            ))
        ),
        'SOURCE': _Field(
            required=False,
            single_record=False,
            body=True,
            post_processor=('lambda', lambda field_vals, all_subfields: [
                (field_val.combined_header, *subfields['ORGANISM'])
                for field_val, subfields in zip(field_vals, all_subfields)
            ]),
            subfields={
                'ORGANISM': _Field(
                    required=True,
                    single_record=True,
                    body=False,
                    post_processor=('lambda', lambda lines: (lines[0].strip(),
                                                             split_semicolon_list(combine_lines(lines[1:]))))
                )
            }
        ),
        'REFERENCE': _Field(
            required=True,
            single_record=False,
            body=True,
            post_processor=('method', '_parse_reference_fields'),
            subfields={
                'AUTHORS': _Field(
                    required=True,
                    single_record=True,
                    body=False,
                    post_processor=('lambda', combine_lines)
                ),
                'CONSRTM': _Field(
                    required=False,
                    single_record=True,
                    body=False,
                    post_processor=('lambda', combine_lines)
                ),
                'TITLE': _Field(
                    required=False,
                    single_record=True,
                    body=False,
                    post_processor=('lambda', combine_lines)
                ),
                'JOURNAL': _Field(
                    required=True,
                    single_record=True,
                    body=False,
                    post_processor=('lambda', combine_lines)
                ),
                'MEDLINE': _Field(
                    required=False,
                    single_record=True,
                    body=False,
                    post_processor=NotImplemented
                ),
                'PUBMED': _Field(
                    required=False,
                    single_record=True,
                    body=False,
                    post_processor=('standard', 'one_line_header')
                ),
                'REMARK': _Field(
                    required=False,
                    single_record=True,
                    body=False,
                    post_processor=('lambda', combine_lines)
                ),
            }
        ),
        'COMMENT': _Field(
            required=False,
            single_record=False,
            body=False,
            post_processor=('lambda', lambda records: [list(map(str.strip, lines)) for lines in records])
        ),
        'FEATURES': _Field(
            required=True,
            single_record=True,
            body=True,
            post_processor=('method', '_parse_features_field')
        ),
        'CONTIG': _Field(
            required=False,
            single_record=True,
            body=False,
            post_processor=NotImplemented
        ),
        'ORIGIN': _Field(
            required=True,
            single_record=True,
            body=True,
            post_processor=('standard', 'body_only')
        )
    }

    @dataclass(frozen=True)
    class _FileHeaderInfo:
        file_name: str
        database_name: str
        release_date: date
        flatfile_version: tuple[int, int]
        title: str

    def __init__(self, filename):
        self._filename = filename
        self._f = None
        self._file_header_info: Optional[GenBankFlatfile._FileHeaderInfo] = None

    def _parse_file_header(self):
        assert self._file_header_info is None

        file_name, database_name = next(self._f).split(maxsplit=1)

        release_date = datetime.strptime(next(self._f).strip(), '%B %d %Y').date()

        assert next(self._f) == '\n'

        major_version_txt, minor_version_txt = next(self._f).split('.')
        _, major_version_str = major_version_txt.rsplit(' ', 1)
        minor_version = int(minor_version_txt)

        assert next(self._f) == '\n'

        title = next(self._f).strip()

        assert next(self._f) == '\n'

        # TODO: parse number of loci/bases/sequences
        next(self._f)

        assert next(self._f) == '\n'

        self._file_header_info = GenBankFlatfile._FileHeaderInfo(
            file_name=file_name,
            database_name=database_name.strip(),
            release_date=release_date,
            flatfile_version=(int(major_version_str), minor_version),
            title=title
        )

    @property
    def file_name(self) -> Optional[str]:
        if self._file_header_info is None:
            return None
        return self._file_header_info.file_name

    @property
    def database_name(self) -> Optional[str]:
        if self._file_header_info is None:
            return None
        return self._file_header_info.database_name

    @property
    def release_date(self) -> Optional[date]:
        if self._file_header_info is None:
            return None
        return self._file_header_info.release_date

    @property
    def flatfile_version(self) -> Optional[tuple[int, int]]:
        if self._file_header_info is None:
            return None
        return self._file_header_info.flatfile_version

    @property
    def title(self) -> Optional[str]:
        if self._file_header_info is None:
            return None
        return self._file_header_info.title

    def __enter__(self):
        self._f = open(self._filename)
        try:
            self._parse_file_header()
            if self._file_header_info.flatfile_version[0] != 258:
                warnings.warn('This program was designed for GenBank release 258, '
                              f'but this file is version {self._file_header_info.flatfile_version[0]}'
                              f'.{self._file_header_info.flatfile_version[1]}')
        except Exception:
            self._f.close()
            raise
        return self

    def __exit__(self, *exc):
        self._f.close()
        return False

    def __iter__(self):
        if self._f is None:
            raise RuntimeError('GenBankFlatfile must be used as a context manager')
        return self

    def __next__(self):
        for line in self._f:
            if line.startswith('LOCUS'):
                # if not line.startswith('LOCUS       AB180941'):
                #     continue
                raw_fields = self.get_locus_raw_fields(line)
                parsed_fields = self.parse_fields(fields_decl=GenBankFlatfile._FIELDS,
                                                  raw_fields=raw_fields)
                return GenBankLocus(
                    name=parsed_fields['LOCUS']['locus_name'],
                    accession=parsed_fields['ACCESSION'],
                    version=parsed_fields['VERSION'],
                    sequence_length=parsed_fields['LOCUS']['sequence_length'],
                    molecule_type=parsed_fields['LOCUS']['molecule_type'],
                    organism_division=parsed_fields['LOCUS']['organism_division'],
                    topology=parsed_fields['LOCUS']['molecule_topology'],
                    date=parsed_fields['LOCUS']['date'],
                    dblink=parsed_fields['DBLINK'] if 'DBLINK' in parsed_fields else (),

                    sequence=GenBankFlatfile.parse_sequence(parsed_fields['ORIGIN']),

                    definition=parsed_fields['DEFINITION'],
                    source=parsed_fields['SOURCE'],
                    references=parsed_fields['REFERENCE'],

                    feature_table=parsed_fields['FEATURES'],

                    comment=parsed_fields['COMMENT'] if 'COMMENT' in parsed_fields else (),
                    keywords=parsed_fields['KEYWORDS']
                )
        raise StopIteration

    def get_locus_raw_fields(self, start_line):
        match = re.fullmatch(r'(LOCUS +)(.*\n)', start_line)
        assert match is not None
        continued_val_prefix_len = len(match.group(1))
        continued_val_prefix = ' ' * continued_val_prefix_len
        locus_val = match.group(2)

        attributes = defaultdict(list)
        attributes['LOCUS'].append(AttrVal(header_lines=[locus_val]))
        last_key = 'LOCUS'
        for line in self._f:
            if line.startswith(continued_val_prefix):
                if attributes[last_key][-1].body:
                    # we have already encountered the body, so add to that
                    attributes[last_key][-1].body.append(line)
                else:
                    # this line is a continuation of the header
                    attributes[last_key][-1].header_lines.append(line[continued_val_prefix_len:])
            elif line.startswith(' '):

                # have to fix the indentation of PUBMED
                if ' PUBMED' in line[:12]:
                    line = line.replace(' PUBMED', 'PUBMED ')

                attributes[last_key][-1].body.append(line)
            elif line == '//\n':
                break
            else:
                split_line = line.split(maxsplit=1)
                if len(split_line) == 1:
                    last_key, = split_line
                    header_lines = None
                else:
                    last_key, key_val = split_line
                    header_lines = [key_val]
                attributes[last_key].append(AttrVal(header_lines=header_lines))
        return attributes

    def parse_fields(self, fields_decl, raw_fields: dict[str, list[AttrVal]]) -> dict[str, Any]:
        parsed_fields = {}
        for field_name, field_decl in fields_decl.items():
            if field_decl.required:
                assert field_name in raw_fields
            elif field_name not in raw_fields:
                continue
            raw_field_val = raw_fields.pop(field_name)

            if field_decl.single_record:
                assert len(raw_field_val) == 1
                if field_decl.body:
                    post_processor_args = [raw_field_val[0]]
                else:
                    post_processor_args = [raw_field_val[0].header_lines]
            else:
                if field_decl.body:
                    post_processor_args = [raw_field_val]
                else:
                    post_processor_args = [[single_raw_field_val.header_lines
                                            for single_raw_field_val in raw_field_val]]

            if field_decl.body:
                assert all(raw_single_field_val.body for raw_single_field_val in raw_field_val)
            else:
                assert not any(raw_single_field_val.body for raw_single_field_val in raw_field_val)

            if field_decl.subfields is not None:
                parsed_subfields = []
                for single_raw_field_val in raw_field_val:
                    single_raw_subfields = self.parse_subkeys(single_raw_field_val.body)
                    parsed_subfields.append(self.parse_fields(fields_decl=field_decl.subfields,
                                                              raw_fields=single_raw_subfields))
                if field_decl.single_record:
                    assert len(parsed_subfields) == 1
                    post_processor_args.append(parsed_subfields[0])
                else:
                    post_processor_args.append(parsed_subfields)

            post_processor_type = field_decl.post_processor[0]
            if post_processor_type == 'standard':
                post_processor = field_decl.post_processor[1]
                if post_processor == 'one_line_header':
                    assert field_decl.single_record
                    assert len(raw_field_val[0].header_lines) == 1
                    parsed_fields[field_name] = raw_field_val[0].header_lines[0].strip()
                elif post_processor == 'body_only':
                    assert field_decl.single_record
                    assert field_decl.body
                    assert raw_field_val[0].header_lines is None
                    parsed_fields[field_name] = raw_field_val[0].body
                else:
                    raise AssertionError(f'Invalid field post processor for {field_name}: {post_processor}')
            else:
                if post_processor_type == 'lambda':
                    post_processor_func: Callable = field_decl.post_processor[1]
                elif post_processor_type == 'method':
                    post_processor_func: Callable = getattr(self, field_decl.post_processor[1])
                else:
                    raise AssertionError(f'Invalid field post processor for {field_name}: {post_processor_type}')

                parsed_fields[field_name] = post_processor_func(*post_processor_args)

            # print(f'{field_name}={parsed_fields[field_name]!r}')

        assert not raw_fields
        return parsed_fields

    @staticmethod
    def parse_subkeys(lines):
        attributes = defaultdict(list)
        for key, val_lines in parse_indented_keyvalue(lines):
            attributes[key].append(AttrVal(header_lines=val_lines))
        return attributes

    @staticmethod
    def parse_sequence(lines):
        sequence = ''
        for line in lines:
            index, line_seq_str = line.split(maxsplit=1)
            assert int(index) == len(sequence) + 1
            sequence += line_seq_str.strip().replace(' ', '')
        return Sequence(sequence, validate=True)

    @staticmethod
    def combine_multiline_field(field_val):
        first_line, other_lines = field_val
        return ' '.join(map(str.strip, [first_line] + other_lines))

    @staticmethod
    def _parse_locus_field(locus_field_lines: list[str]):
        assert len(locus_field_lines) == 1
        (locus_name, seq_len_str, bp_str, mol_type_str,
         topology_str, div_code_str, date_str) = locus_field_lines[0].split()
        assert bp_str == 'bp'
        if '-' in mol_type_str:
            raise NotImplementedError(f'{locus_name}: strandedness is not implemented')
        else:
            mol_type = GenBankMoleculeType(mol_type_str)
        return {
            'locus_name': locus_name,
            'sequence_length': int(seq_len_str),
            'molecule_type': mol_type,
            'molecule_topology': GenBankTopology(topology_str),
            'organism_division': GenBankOrganismDivision(div_code_str),
            'date': datetime.strptime(date_str, '%d-%b-%Y').date()
        }

    @staticmethod
    def _parse_reference_fields(field_vals: list[AttrVal], all_subfields: list[dict[str, Any]]):
        references_dict = {}
        for field_val, subfields in zip(field_vals, all_subfields):
            match = re.fullmatch(r'([0-9]+)(?: +\(bases ([0-9]+) to ([0-9]+)\))?', field_val.one_line_header)
            assert match is not None
            reference_num = int(match.group(1))
            if match.group(2) is None:
                reference_seq_range = None
            else:
                reference_seq_range = tuple(map(int, match.group(2, 3)))
            references_dict[reference_num] = (reference_seq_range, GenBankReference(
                authors=subfields['AUTHORS'],
                consortiums=subfields.get('CONSRTM'),
                title=subfields.get('TITLE'),
                journal=subfields['JOURNAL'],
                pubmed_id=int(subfields['PUBMED']) if 'PUBMED' in subfields else None,
                remark=subfields.get('REMARK')
            ))
        reference_num_range = range(1, max(references_dict.keys()) + 1)
        assert references_dict.keys() == set(reference_num_range)
        return [references_dict[i] for i in reference_num_range]

    @staticmethod
    def _parse_dblink_field(dblink_field_vals: list[AttrVal]):
        dblink_cross_references = []
        for dblink_lines in dblink_field_vals:
            for dblink_line in dblink_lines:
                cr_type, cr_id_str = dblink_line.split(':')
                dblink_cross_references.append((cr_type, tuple(cr_id_str.strip().split(','))))
        return tuple(dblink_cross_references)

    @staticmethod
    def _parse_features_field(features_attribute: AttrVal):
        assert features_attribute.one_line_header == 'Location/Qualifiers'
        return parse_feature_table(lines=features_attribute.body)


def main():
    # genbank_flatfile('../data/gbbct1.seq')

    # from genbank.file import File
    #
    # file = File('../data/gbbct1.seq')
    # for locus in file:
    #     print(locus)
    #     break

    from pprint import pprint
    with GenBankFlatfile('../data/gbbct1.seq') as f:
        # print(f'{f.file_name=}')
        # print(f'{f.database_name=}')
        # print(f'{f.release_date=}')
        # print(f'{f.flatfile_version=}')
        # print(f'{f.title=}')
        for i, locus in enumerate(f):
            pprint(locus)
            print(i)
            if i == 2246:
                break


if __name__ == '__main__':
    main()
