#!/usr/bin/env python3
from __future__ import annotations

import dataclasses
from dataclasses import dataclass
from enum import Enum, EnumMeta
import re

AMINO_ACID_ONE_LETTER_ABBREVIATIONS = 'ARNDCQEGHILKMFPOSUTWYVBZXJ'


def combine_lines(lines):
    return ' '.join(map(str.strip, lines))


@dataclass(frozen=True)
class AttrVal:
    header_lines: list[str]
    body: list[str] = dataclasses.field(default_factory=list)

    def __iter__(self):
        # to make it unpackable
        return iter((self.header_lines, self.body))

    @property
    def one_line_header(self):
        assert len(self.header_lines) == 1
        return self.header_lines[0].strip()

    @property
    def combined_header(self):
        return combine_lines(self.header_lines)


def parse_indented_keyvalue(lines: list[str]):
    if not lines:
        return
    first_line = lines[0]
    prefix_spaces = len(first_line) - len(first_line.lstrip(' '))
    prefix = ' ' * prefix_spaces
    assert not first_line[prefix_spaces:].startswith(' ')
    split_first_line = first_line.split(maxsplit=1)
    if len(split_first_line) == 1:
        val_start_col = None
    else:
        _, first_line_val = first_line.split(maxsplit=1)
        val_start_col = first_line.index(first_line_val) - prefix_spaces

    cur_key = None
    cur_val_lines = None
    for line in lines:
        assert line.startswith(prefix)
        assert line.endswith('\n')
        line = line[prefix_spaces:-1]
        if line.startswith(' '):
            if val_start_col is None:
                val_start_col = len(line) - len(line.lstrip(' '))
            assert line[:val_start_col] == ' ' * val_start_col
            cur_val_lines.append(line[val_start_col:])
        else:
            if cur_key is not None:
                assert cur_val_lines is not None
                yield cur_key, cur_val_lines
            split_line = line.split(maxsplit=1)
            if len(split_line) == 1:
                cur_key, = split_line
                cur_val_lines = ['']
            else:
                cur_key, val = split_line
                cur_val_lines = [val]
    if cur_key is not None:
        assert cur_val_lines is not None
        yield cur_key, cur_val_lines


class _CaseInsensitiveEnumMeta(EnumMeta):

    def __new__(metacls, name, bases, *args, **kwargs):
        assert issubclass(bases[0], str)
        return super().__new__(metacls, name, bases, *args, **kwargs)

    def __init__(cls, *args, **kwargs):
        # TODO: disallow _lowercase_member_map and regex_pattern members
        super().__init__(*args, **kwargs)
        cls._lowercase_value2member_map = {member.lower(): member for member in cls}
        cls.regex_pattern = '(?i:' + '|'.join(re.escape(s) for s in cls) + ')'

    def __call__(cls, value, *args, **kwargs):
        if args or kwargs:
            return super().__call__(value, *args, **kwargs)
        if isinstance(value, str):
            return cls._lowercase_value2member_map[value.lower()]
        return super().__call__(value)

    def __contains__(cls, member):
        if isinstance(member, cls):
            return True
        if not isinstance(member, str):
            raise TypeError(f'{cls.__name__} members are strings, not {type(member)}')
        return member.lower() in cls._lowercase_value2member_map


class CaseInsensitiveEnum(str, Enum, metaclass=_CaseInsensitiveEnumMeta):
    pass
