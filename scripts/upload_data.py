#!/usr/bin/env python3
import requests

from genbank_parser import GenBankFlatfile


def main():
    with GenBankFlatfile('../data/gbbct1.seq') as f:
        for i, locus in enumerate(f):
            if i > 2245:
                break
            # if i > 100:
            #     break

            response = requests.post('http://localhost:6201/api/upload', json={
                'fasta': locus.to_fasta(),
                'metadata': {
                    'name': locus.name,
                    'organism': locus.source[0][1] + '\n' + '; '.join(locus.source[0][2]),
                    'organism_confidence': 'imported_from_database',
                    'comment': ''  # TODO
                }
            })
            try:
                response.raise_for_status()
            except requests.exceptions.HTTPError:
                print(f'HTTP {response.status_code} for {locus.name}: {response.text}')
            else:
                print(f'{locus.name}: {response.text}')


if __name__ == '__main__':
    main()
