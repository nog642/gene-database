#!/usr/bin/env python3
import requests


def download_http(url, output_path):
    response = requests.get(url)
    response.raise_for_status()
    with open(output_path, 'wb') as f:
        f.write(response.content)


download_http('https://ftp.ncbi.nlm.nih.gov/genbank/gbbct1.seq.gz', '../data/gbbct1.seq.gz')
