#!/usr/bin/env python3
from dataclasses import dataclass
from datetime import date
from enum import Enum
import sys
from typing import Optional

sys.path.append('../server/webserver/app')
from Sequence import Sequence
from feature_table_datastructures import FeatureTable


class GenBankMoleculeType(Enum):
    NA = 'NA'
    DNA = 'DNA'
    RNA = 'RNA'
    tRNA = 'tRNA'
    rRNA = 'rRNA'
    mRNA = 'mRNA'
    uRNA = 'uRNA'
    cRNA = 'cRNA'


class GenBankOrganismDivision(Enum):
    Bacterial = 'BCT'
    Primate = 'PRI'
    Rodent = 'ROD'
    OtherMammal = 'MAM'
    OtherVertebrate = 'VRT'
    Invertebrate = 'INV'
    PlantOrFungal = 'PLN'
    Viral = 'VRL'
    Phage = 'PHG'
    StructuralRNA = 'RNA'
    Synthetic = 'SYN'
    Unannotated = 'UNA'


class GenBankTopology(Enum):
    linear = 'linear'
    circular = 'circular'


@dataclass(frozen=True)
class GenBankReference:
    authors: str
    consortiums: Optional[str]
    title: Optional[str]
    journal: str
    pubmed_id: Optional[int]
    remark: Optional[str]


@dataclass(frozen=True)
class GenBankLocus:
    name: str
    accession: tuple[str, ...]
    version: str
    sequence_length: int
    molecule_type: GenBankMoleculeType
    organism_division: GenBankOrganismDivision
    topology: GenBankTopology
    date: date
    dblink: tuple[tuple[str, tuple[str, ...]], ...]

    sequence: Sequence

    definition: str
    source: list[str, str, tuple[str, ...]]
    references: list[tuple[Optional[tuple[int, int]], GenBankReference]]

    feature_table: FeatureTable

    keywords: tuple[str, ...]
    comment: list[list[str]]

    def to_fasta(self):
        return self.sequence.to_fasta(self.name)
