#!/usr/bin/env python3
from __future__ import annotations

from enum import Enum, EnumMeta
import itertools
import re
from typing import ClassVar

from util import CaseInsensitiveEnum


class DBXRefDatabase(CaseInsensitiveEnum):
    AceView_WormGenes = 'AceView/WormGenes'
    AFTOL = 'AFTOL'
    AntWeb = 'AntWeb'
    APHIDBASE = 'APHIDBASE'
    ApiDB = 'ApiDB'
    ApiDB_CryptoDB = 'ApiDB_CryptoDB'
    ApiDB_PlasmoDB = 'ApiDB_PlasmoDB'
    ApiDB_ToxoDB = 'ApiDB_ToxoDB'
    Araport = 'Araport'
    ASAP = 'ASAP'
    ATCC = 'ATCC'
    ATCC_in_host = 'ATCC(in host)'
    ATCC_dna = 'ATCC(dna)'
    Axeldb = 'Axeldb'
    BDGP_EST = 'BDGP_EST'
    BDGP_INS = 'BDGP_INS'
    BEEBASE = 'BEEBASE'
    BEETLEBASE = 'BEETLEBASE'
    BEI = 'BEI'
    BGD = 'BGD'
    Bold = 'Bold'
    CABRI = 'CABRI'
    CCAP = 'CCAP'
    CDD = 'CDD'
    CGD = 'CGD'
    dbEST = 'dbEST'
    dbProbe = 'dbProbe'
    dbSNP = 'dbSNP'
    dbSTS = 'dbSTS'
    dictyBase = 'dictyBase'
    EcoGene = 'EcoGene'
    ECOCYC = 'ECOCYC'
    ENSEMBL = 'ENSEMBL'
    EnsemblGenomes = 'EnsemblGenomes'
    EPD = 'EPD'
    ERIC = 'ERIC'
    ESTLIB = 'ESTLIB'
    FANTOM_DB = 'FANTOM_DB'
    FBOL = 'FBOL'
    FLYBASE = 'FLYBASE'
    Fungorum = 'Fungorum'
    GABI = 'GABI'
    GDB = 'GDB'
    GeneDB = 'GeneDB'
    GeneID = 'GeneID'
    GI = 'GI'
    GO = 'GO'
    GOA  = 'GOA '
    Greengenes = 'Greengenes'
    GRIN = 'GRIN'
    HGNC = 'HGNC'
    H_InvDB = 'H-InvDB'
    HMP = 'HMP'
    HOMD = 'HOMD'
    HPM = 'HPM'
    HSSP = 'HSSP'
    IKMC = 'IKMC'
    IMGT_GENE_DB = 'IMGT/GENE-DB'
    IMGT_LIGM = 'IMGT/LIGM'
    IMGT_HLA = 'IMGT/HLA'
    InterPro = 'InterPro'
    IntrepidBio = 'IntrepidBio'
    IRD = 'IRD'
    ISFinder = 'ISFinder'
    ISHAM_ITS = 'ISHAM-ITS'
    JCM = 'JCM'
    JGIDB = 'JGIDB'
    Phytozome = 'Phytozome'
    Phytozme = 'Phytozme'
    LocusID = 'LocusID'
    MaizeGDB = 'MaizeGDB'
    MarpolBase = 'MarpolBase'
    MedGen = 'MedGen'
    MGI = 'MGI'
    MIM = 'MIM'
    miRBase = 'miRBase'
    MycoBank = 'MycoBank'
    NBRC = 'NBRC'
    NextDB = 'NextDB'
    niaEST = 'niaEST'
    NMPDR = 'NMPDR'
    NRESTdb = 'NRESTdb'
    OrthoMCL = 'OrthoMCL'
    Osa1 = 'Osa1'
    Pathema = 'Pathema'
    PBmice = 'PBmice'
    PDB = 'PDB'
    PFAM = 'PFAM'
    PGN = 'PGN'
    PIR = 'PIR'
    PomBase = 'PomBase'
    PSEUDO = 'PSEUDO'
    PseudoCap = 'PseudoCap'
    RAP_DB = 'RAP-DB'
    RATMAP = 'RATMAP'
    RBGE_garden = 'RBGE_garden'
    RBGE_herbarium = 'RBGE_herbarium'
    RFAM = 'RFAM'
    RGD = 'RGD'
    RiceGenes = 'RiceGenes'
    RNAcentral = 'RNAcentral'
    RZPD = 'RZPD'
    SEED = 'SEED'
    SGD = 'SGD'
    SGN = 'SGN'
    SK_FST = 'SK-FST'
    SoyBase = 'SoyBase'
    SRPDB = 'SRPDB'
    SubtiList = 'SubtiList'
    taxon = 'taxon'
    TAIR = 'TAIR'
    TIGRFAM = 'TIGRFAM'
    TubercuList = 'TubercuList'
    UNILIB = 'UNILIB'
    UniProtKB_Swiss_Prot = 'UniProtKB/Swiss-Prot'
    UniProtKB_TrEMBL = 'UniProtKB/TrEMBL'
    UniSTS = 'UniSTS'
    UNITE = 'UNITE'
    VBASE2 = 'VBASE2'
    VectorBase = 'VectorBase'
    VGNC = 'VGNC'
    ViPR = 'ViPR'
    WorfDB = 'WorfDB'
    WormBase = 'WormBase'
    Xenbase = 'Xenbase'
    ZFIN = 'ZFIN'


class TypeMaterial(CaseInsensitiveEnum):
    """
    https://www.insdc.org/submitting-standards/controlled-vocabulary-typematerial-qualifer/
    """
    SYNONYMS: ClassVar[dict[str, TypeMaterial]]
    regex_pattern: ClassVar[str]

    type_material = 'type material'
    type_strain = 'type strain'
    reference_material = 'reference material'
    reference_strain = 'reference strain'
    neotype_strain = 'neotype strain'
    holotype = 'holotype'
    isotype = 'isotype'
    paratype = 'paratype'
    neotype = 'neotype'
    epitype = 'epitype'
    syntype = 'syntype'
    isosyntype = 'isosyntype'
    lectotype = 'lectotype'
    paralectotype = 'paralectotype'
    hapantotype = 'hapantotype'
    allotype = 'allotype'
    isoparatype = 'isoparatype'
    isoneotype = 'isoneotype'
    isolectotype = 'isolectotype'
    isoepitype = 'isoepitype'
    culture_from_reference = 'culture from reference'
    culture_from_type_material = 'culture from type material'
    culture_from_paratype = 'culture from paratype'
    culture_from_neotype = 'culture from neotype'
    culture_from_lectotype = 'culture from lectotype'
    culture_from_hapantotype = 'culture from hapantotype'
    culture_from_isosyntype = 'culture from isosyntype'
    culture_from_isotype = 'culture from isotype'
    culture_from_isoparatype = 'culture from isoparatype'
    culture_from_isoneotype = 'culture from isoneotype'
    culture_from_isolectotype = 'culture from isolectotype'
    culture_from_holotype = 'culture from holotype'
    culture_from_epitype = 'culture from epitype'
    culture_from_syntype = 'culture from syntype'
    culture_from_isoepitype = 'culture from isoepitype'
    pathotype_strain = 'pathotype strain'

    @classmethod
    def _missing_(cls, value):
        if not isinstance(value, str):
            raise TypeError('value must be a string')
        if value.lower() in TypeMaterial.SYNONYMS:
            return TypeMaterial.SYNONYMS[value]


TypeMaterial.SYNONYMS = {
    'ex-type': TypeMaterial.culture_from_type_material,
    'ex-paratype': TypeMaterial.culture_from_paratype,
    'ex-neotype': TypeMaterial.culture_from_neotype,
    'ex-lectotype': TypeMaterial.culture_from_lectotype,
    'ex-isosyntype': TypeMaterial.culture_from_isosyntype,
    'ex-isotype': TypeMaterial.culture_from_isotype,
    'ex-isoparatype': TypeMaterial.culture_from_isoparatype,
    'ex-isoneotype': TypeMaterial.culture_from_isoneotype,
    'ex-isolectotype': TypeMaterial.culture_from_isolectotype,
    'ex-holotype': TypeMaterial.culture_from_holotype,
    'ex-epitype': TypeMaterial.culture_from_epitype,
    'ex-syntype': TypeMaterial.culture_from_syntype,
    'ex-isoepitype': TypeMaterial.culture_from_isoepitype
}
# we need these to all be lowercase because that is what they are compared against,
#     to support case insensitivity
assert all(type_material_synonym == type_material_synonym.lower()
           for type_material_synonym in TypeMaterial.SYNONYMS)
TypeMaterial.regex_pattern = '|'.join(
    re.escape(s) for s in itertools.chain(TypeMaterial, TypeMaterial.SYNONYMS.keys())
)


class RegulatoryClass(CaseInsensitiveEnum):
    """
    https://www.insdc.org/submitting-standards/controlled-vocabulary-regulatoryclass/
    """
    attenuator = 'attenuator'
    CAAT_signal = 'CAAT_signal'
    DNase_I_hypersensitive_site = 'DNase_I_hypersensitive_site'
    enhancer = 'enhancer'
    enhancer_blocking_element = 'enhancer_blocking_element'
    GC_signal = 'GC_signal'
    imprinting_control_region = 'imprinting_control_region'
    insulator = 'insulator'
    locus_control_region = 'locus_control_region'
    matrix_attachment_region = 'matrix_attachment_region'
    minus_35_signal = 'minus_35_signal'
    minus_10_signal = 'minus_10_signal'
    polyA_signal_sequence = 'polyA_signal_sequence'
    promoter = 'promoter'
    recoding_stimulatory_region = 'recoding_stimulatory_region'
    recombination_enhancer = 'recombination_enhancer'
    replication_regulatory_region = 'replication_regulatory_region'
    response_element = 'response_element'
    ribosome_binding_site = 'ribosome_binding_site'
    riboswitch = 'riboswitch'
    silencer = 'silencer'
    TATA_box = 'TATA_box'
    terminator = 'terminator'
    transcriptional_cis_regulatory_region = 'transcriptional_cis_regulatory_region'
    uORF = 'uORF'
    other = 'other'


class RepeatType(CaseInsensitiveEnum):
    tandem = 'tandem'
    direct = 'direct'
    inverted = 'inverted'
    flanking = 'flanking'
    nested = 'nested'
    dispersed = 'dispersed'
    terminal = 'terminal'
    long_terminal_repeat = 'long_terminal_repeat'
    non_ltr_retrotransposon_polymeric_tract = 'non_ltr_retrotransposon_polymeric_tract'
    centromeric_repeat = 'centromeric_repeat'
    telomeric_repeat = 'telomeric_repeat'
    x_element_combinatorial_repeat = 'x_element_combinatorial_repeat'
    y_prime_element = 'y_prime_element'
    other = 'other'


class MobileElementType(CaseInsensitiveEnum):
    transposon = 'transposon'
    retrotransposon = 'retrotransposon'
    integron = 'integron'
    insertion_sequence = 'insertion sequence'
    non_LTR_retrotransposon = 'non-LTR retrotransposon'
    SINE = 'SINE'
    MITE = 'MITE'
    LINE = 'LINE'
    other = 'other'


class InferenceCategory(CaseInsensitiveEnum):
    COORDINATES = 'COORDINATES'
    DESCRIPTION = 'DESCRIPTION'
    EXISTENCE = 'EXISTENCE'


class InferenceType(CaseInsensitiveEnum):
    # TODO: handle the hierarchical structure between these
    non_experimental_evidence = 'non-experimental evidence, no additional details recorded'
    similar_to_sequence = 'similar to sequence'
    similar_to_sequence_AA = 'similar to AA sequence'
    similar_to_sequence_DNA = 'similar to DNA sequence'
    similar_to_sequence_RNA = 'similar to RNA sequence'
    similar_to_sequence_mRNA = 'similar to RNA sequence, mRNA'
    similar_to_sequence_EST = 'similar to RNA sequence, EST'
    similar_to_sequence_other_RNA = 'similar to RNA sequence, other RNA'
    profile = 'profile'
    nucleotide_motif = 'nucleotide motif'
    protein_motif = 'protein motif'
    ab_initio_prediction = 'ab initio prediction'
    alignment = 'alignment'
