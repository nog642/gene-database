#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
set -x
sudo rm -r "$SCRIPT_DIR/data/db"
sudo rm -r "$SCRIPT_DIR/data/configdb"
mkdir "$SCRIPT_DIR/data/db"
mkdir "$SCRIPT_DIR/data/configdb"
