#!/usr/bin/env python3
import os
import sys

from nog642_util import get_full_traceback, validate_schema

from GeneralizedApplication import (GeneralizedApplication, GASuccess, BadRequest)
from schemas import (UPLOAD_REQUEST_SCHEMA, COMMENT_REQUEST_SCHEMA, VOTE_REQUEST_SCHEMA,
                     SEQUENCE_RESPOSNE_SCHEMA, REPLY_REQUEST_SCHEMA)
from Sequence import Sequence
from database import GeneDatabase


def load_static_files():
    static_files = {}
    for filename in os.listdir('static'):
        with open(os.path.join('static', filename)) as f:
            content = f.read()
        static_files[filename] = content
    return static_files


class GeneDatabaseWebApp(GeneralizedApplication):

    def __init__(self):
        self.static_files = load_static_files()
        super().__init__()
        self.gene_db = GeneDatabase()

    ENDPOINTS = {
        ('POST', '/api/upload'): {
            'method': 'post_upload',
            'query_parameters': {},
            'body_json': True,
            'body_schema': UPLOAD_REQUEST_SCHEMA
        },
        ('POST', '/api/comment'): {
            'method': 'post_comment',
            'query_parameters': {},
            'body_json': True,
            'body_schema': COMMENT_REQUEST_SCHEMA
        },
        ('GET', '/api/get_seq_data'): {
            'method': 'get_sequence_data',
            'query_parameters': {
                'seq': {
                    'required': True,
                    'argument': 'seq_hash'
                }
            },
            'body_json': False
        },
        ('GET', '/api/search'): {
            'method': 'search',
            'query_parameters': {},  # TODO
            'body_json': False
        },
        ('POST', '/api/vote'): {
            'method': 'post_vote',
            'query_parameters': {},
            'body_json': True,
            'json_schema': VOTE_REQUEST_SCHEMA
        },
        ('POST', '/api/reply'): {
            'method': 'post_reply',
            'query_parameters': {},
            'body_json': True,
            'json_schema': REPLY_REQUEST_SCHEMA
        }
    }
    STATIC_FILES = {  # GET URL path -> (filepath, content type)
        '/': ('home.html', 'text/html'),
        '/upload': ('upload.html', 'text/html'),
        '/seqsearch': ('seqsearch.html', 'text/html'),
        '/search': ('searchresults.html', 'text/html'),
        '/seq/<seq_hash>': ('sequence.html', 'text/html'),
        '/style.css': ('style.css', 'text/css'),
        '/searchresults.js': ('searchresults.js', 'text/javascript'),
        '/topbar.js': ('topbar.js', 'text/javascript'),
        '/upload.js': ('upload.js', 'text/javascript'),
        '/sequence.js': ('sequence.js', 'text/javascript'),
        '/utils.js': ('utils.js', 'text/javascript'),
    }

    def post_upload(self, body):
        if '-' in body['fasta']:
            raise BadRequest('Indeterminate length gaps (-) are not supported yet.'
                             'Consider using single-nucleotide gaps (n) instead.')
        try:
            sequence = Sequence.from_fasta(body['fasta'])
        except ValueError as e:
            raise BadRequest(str(e)) from e
        success, error_message = self.gene_db.add_sequence(sequence=sequence, metadata=body['metadata'])
        if not success:
            raise BadRequest(error_message)
        return GASuccess(response_data=sequence.sha256_hash)

    def post_comment(self, body):
        result, error_message = self.gene_db.add_identification(seq_hash=body['hash'],
                                                                identification=body['identification'])
        if result is None:
            raise BadRequest(error_message)
        return GASuccess(None)

    def get_sequence_data(self, seq_hash):
        try:
            seq_data, error_message = self.gene_db.get_sequence(seq_hash)
            if seq_data is None:
                raise BadRequest(error_message)
            seq_data['sequence'] = seq_data['sequence'].to_fasta(seq_data['metadata']['name'])
        except Exception:
            print(get_full_traceback(), file=sys.stderr)
            raise BadRequest("There was an error.")
        validate_schema(data=seq_data, schema=SEQUENCE_RESPOSNE_SCHEMA)
        return GASuccess(seq_data)

    def search(self):
        # TODO: actually implement search
        retval = [v for _, v in zip(range(20), self.gene_db.get_all_sequences())]
        return GASuccess(retval)

    def post_vote(self, body):
        error_message = self.gene_db.vote_identification(
            seq_hash=body['hash'],
            identification=body['identification'],
            upvotes=body['upvote'],
            downvotes=body['downvote']
        )
        if error_message is not None:
            raise BadRequest(error_message)
        return GASuccess(None)

    def post_reply(self, body):
        error_message = self.gene_db.add_reply(
            seq_hash=body['hash'],
            identification=body['identification'],
            comment=body['reply']
        )
        if error_message is not None:
            raise BadRequest(error_message)
        return GASuccess(None)


def create_app():
    return GeneDatabaseWebApp().app


if __name__ == '__main__':
    create_app().run(host='0.0.0.0', port=6201)
