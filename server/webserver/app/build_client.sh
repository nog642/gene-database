#!/bin/sh
set -e
cd client
node_modules/typescript/bin/tsc
cd ../static
ln -s ../client/dist/*.js ./
