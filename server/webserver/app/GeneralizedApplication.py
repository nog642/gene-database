#!/usr/bin/env python3
import dataclasses
from dataclasses import dataclass
from http import HTTPStatus
import sys
from typing import Any, Optional

import flask
from flask import Flask, request, jsonify
from nog642_util import get_full_traceback, validate_schema, SchemaError


# for use instead of None as a sentinel value, when None may be an actual value
_SENTINEL = object()


def rename(newname):
    def decorator(f):
        f.__name__ = newname
        return f
    return decorator


class _CustomErrorResponseException(Exception):

    def __init__(self, error: str, response_data: Any = _SENTINEL):
        # error is developer facing error message
        self.error = error
        self.response_data = response_data

    @property
    def has_reponse_data(self):
        return self.response_data is not _SENTINEL

    def __str__(self):
        # to match default behavior of Exception if it is not caught by GeneralizedApplication
        return self.error


class BadRequest(_CustomErrorResponseException):
    """
    Raised when the request is bad at an API level (e.g. required parameters). Corresponds to HTTP error 400.
    This is caught in GeneralizedApplication.
    """


class ServerError(_CustomErrorResponseException):
    """
    Raised when there is a server error and you want to send a custom response. Corresponds to HTTP error 500.
    This is caught in GeneralizedApplication.
    """


class NotFoundResponse(_CustomErrorResponseException):
    """
    Raised when the hook wants to return an HTTP 404 error response.
    This is caught in GeneralizedApplication.
    """


@dataclass
class _GAResponse:
    success: bool


@dataclass
class GASuccess(_GAResponse):
    success: bool = dataclasses.field(init=False)
    response_data: Any

    def __post_init__(self):
        self.success = True


@dataclass
class GAFailure(_GAResponse):
    success: bool = dataclasses.field(init=False)
    error_message: Optional[str] = None

    def __post_init__(self):
        self.success = False


class GeneralizedApplication:

    ENDPOINTS = None
    STATIC_FILES = None

    DEFAULT_ERROR_MESSAGE = 'There was an unexpected error'

    # instance attribute
    static_files: Optional[dict] = None

    def __init__(self):
        assert self.ENDPOINTS is not None  # TODO: schema validation
        self.app = self.create_app()

    def _build_response(self, http_status: HTTPStatus, success: bool,
                        error: Optional[str] = None, data=_SENTINEL):
        response = {'success': success}
        if error is not None:
            response['error'] = error
        if data is not _SENTINEL:
            response['data'] = data
        try:
            return jsonify(response), http_status
        except TypeError:
            # can happen if response is not JSON serializable
            return jsonify({
                'success': False,
                'error': self.DEFAULT_ERROR_MESSAGE
            }), HTTPStatus.INTERNAL_SERVER_ERROR

    def _add_flask_route(self, app, method, endpoint, endpoint_def):
        @app.route(endpoint, methods=(method,))
        @rename(endpoint_def['method'])
        def route(**kwargs):

            # TODO: clean this up
            method_kwargs = dict(kwargs)

            for parameter, parameter_def in endpoint_def['query_parameters'].items():
                if parameter_def['required']:
                    # parameter will not be missing unless front end is broken, so try-except is faster
                    try:
                        value = request.args[parameter]
                    except KeyError:
                        # ERROR CONDITION: this code should not run unless there is a bug in the client
                        return jsonify({
                            'success': False,
                            'error': f'Missing required query parameter: {parameter}'
                        }), HTTPStatus.BAD_REQUEST
                else:
                    if 'default' in parameter_def:
                        value = request.args.get(parameter, default=parameter_def['default'])
                    else:
                        # default value is None if the parameter was not passed
                        value = request.args.get(parameter)

                if 'argument' in parameter_def:
                    method_kwargs[parameter_def['argument']] = value

            if endpoint_def['body_json']:

                if 'body_schema' in endpoint_def:
                    try:
                        validate_schema(data=request.json, schema=endpoint_def['body_schema'])
                    except SchemaError as e:
                        return f'Error: {e}', HTTPStatus.BAD_REQUEST

                method_kwargs['body'] = request.json

            try:
                response = getattr(self, endpoint_def['method'])(**method_kwargs)
            except (BadRequest, ServerError, NotFoundResponse) as e:
                if isinstance(e, BadRequest):
                    http_status = HTTPStatus.BAD_REQUEST
                elif isinstance(e, NotFoundResponse):
                    http_status = HTTPStatus.NOT_FOUND
                elif isinstance(e, ServerError):
                    http_status = HTTPStatus.INTERNAL_SERVER_ERROR
                else:
                    # should not be possible since those are the only exceptions we caught
                    raise AssertionError
                response_kwargs = {
                    'http_status': http_status,
                    'success': False,
                    'error': e.error
                }
                if e.has_reponse_data:
                    response_kwargs['data'] = e.response_data
                return self._build_response(**response_kwargs)
            except Exception as e:
                print(f'Error in {request.method} {request.path} call: {e}\n{get_full_traceback()}',
                      file=sys.stderr)
                return jsonify({
                    'success': False,
                    'error': self.DEFAULT_ERROR_MESSAGE
                }), HTTPStatus.INTERNAL_SERVER_ERROR
            if isinstance(response, flask.Response):
                return response
            elif isinstance(response, _GAResponse):
                response_kwargs = {
                    'http_status': HTTPStatus.OK,
                    'success': response.success
                }
                if isinstance(response, GASuccess):
                    if response.response_data is not _SENTINEL:
                        response_kwargs['data'] = response.response_data
                elif isinstance(response, GAFailure):
                    if response.error_message is not None:
                        response_kwargs['error'] = response.error_message
                else:
                    print('_GAResponse object must be either GASuccess or GAFailure', file=sys.stderr)
                    return jsonify({
                        'success': False,
                        'error': self.DEFAULT_ERROR_MESSAGE
                    }), HTTPStatus.INTERNAL_SERVER_ERROR
                return self._build_response(**response_kwargs)
            else:
                print(f'response must be of type flask.Response or GAResponse, not {type(response)}', file=sys.stderr)
                return jsonify({
                    'success': False,
                    'error': self.DEFAULT_ERROR_MESSAGE
                }), HTTPStatus.INTERNAL_SERVER_ERROR

    def _add_static_flask_route(self, app, endpoint, filepath, content_type):
        file_contents = self.static_files[filepath]

        def view_func(**kwargs):
            return flask.Response(file_contents, mimetype=content_type)

        app.add_url_rule(endpoint, endpoint=endpoint, view_func=view_func)

    def create_app(self):
        app = Flask(__name__, instance_relative_config=True)

        for (method, endpoint), endpoint_def in self.ENDPOINTS.items():
            print(f'Creating endpoint: {method} {endpoint}')
            self._add_flask_route(app=app, method=method, endpoint=endpoint,
                                  endpoint_def=endpoint_def)

        if self.STATIC_FILES is not None:
            if not isinstance(self.static_files, dict):
                raise TypeError('self.static_files must be set if STATIC_FILES is set')
            for endpoint, (filepath, content_type) in self.STATIC_FILES.items():
                self._add_static_flask_route(app=app, endpoint=endpoint, filepath=filepath,
                                             content_type=content_type)

        return app

    def run(self, *args, **kwargs):
        # TODO: maybe clean this whole concept up
        return self.app.run(*args, **kwargs)

