#!/usr/bin/env python3
import re
from functools import cached_property
from hashlib import sha256


class Sequence:

    BASE_CODES = {
        'a': {'a'},
        'c': {'c'},
        'g': {'g'},
        't': {'t'},
        'm': {'a', 'c'},
        'r': {'a', 'g'},
        'w': {'a', 't'},
        's': {'c', 'g'},
        'y': {'c', 't'},
        'k': {'g', 't'},
        'v': {'a', 'c', 'g'},
        'h': {'a', 'c', 't'},
        'd': {'a', 'g', 't'},
        'b': {'c', 'g', 't'},
        'n': {'a', 'c', 'g', 't'}
    }

    def __init__(self, sequence, validate=False):
        if validate:
            for i, c in enumerate(sequence):
                if c not in Sequence.BASE_CODES:
                    raise ValueError(f'Invalid char at index {i}: {c!r}')
        self.sequence = sequence

    def __repr__(self):
        return f'Sequence({self.sequence!r})'

    @cached_property
    def sha256_hash(self):
        return sha256(self.sequence.encode()).hexdigest()

    @classmethod
    def from_fasta(cls, str_in):
        match = re.fullmatch(r'>([^\n]*)\n((?i:[acgturykmswbdhvn-]+\n?)+)', str_in.strip())
        if match is None:
            raise ValueError('Invalid FASTA format')
        description, sequence = match.groups()
        sequence = sequence.replace('\n', '').lower().replace('u', 't')
        if '-' in sequence:
            raise NotImplementedError('Indeterminate length gaps (-) are not supported yet.'
                                      'Consider using single-nucleotide gaps (n) instead.')
        return Sequence(sequence, validate=False)

    def to_fasta(self, name):
        line_length = 80
        seq = self.sequence
        return f'>{name}\n' + '\n'.join([seq[i * line_length:(i + 1) * line_length]
                                         for i in range(-(len(seq) // -line_length))])


def test_main():
    seq = Sequence.from_fasta('''
    >NW_022170251.1 Photinus pyralis isolate 1611_PpyrPB1 unplaced genomic scaffold, Ppyr1.3 Ppyr1.4_LG10_unordered_Scaffold1024, whole genome shotgun sequence
TCACCCAAAAAATATTTCGAAACCAAATTTCGTCTTAAATCTTTTCCAATTTGGTATCAGGAGGTTATAA
TGTGCGAGGAGATGTTTCAGCTCCTAATACACGAGGGGACAATCAGAAAATTCTTTTAAAATTTTGTCTG
GCTACCAGGAGAACATAATAGTCGAGGAGGTCTTTTCCCATCACCCACAAAAATATTTCGAAGCCAAATC
TCGTTTTAATCTTTTCTAATTTGGTATCAGGATGTTATAACGTGCGAGGAGGTGTTTCAGCTCCTAACAC
ACGAGGGGAAAATCAGACAATTCTTTTAAAATTTTGTCTGGATATCAGGAGAACATAATAGTCGAGGAGG
TCTTCTCCCATCACCCAGAAAAATATTTCGAAGCCAAATTTCGTTTTAAATTCTTTCCAAT
''')
    print(seq)


if __name__ == '__main__':
    test_main()
