#!/usr/bin/env python
from datetime import datetime
from typing import Optional

from nog642_util import StrEnum, SchemaValueError


class Confidence(StrEnum):
    low = 'low'
    medium = 'medium'
    high = 'high'
    very_high = 'very_high'
    imported_from_database = 'imported_from_database'


METADATA_STORAGE_SCHEMA = (dict, 'explicit', {
    'name': ('required', str),
    'organism': ('required', str),
    'organism_confidence': ('required', Confidence),
    'utc_datetime': ('required', datetime),
})

IDENTIFICATION_STORAGE_SCHEMA = (dict, 'explicit', {
    'author': ('required', Optional[str]),
    'identity': ('required', str),
    'confidence': ('required', Confidence),
    'comment': ('required', str),
    'utc_datetime': ('required', datetime),
    'upvotes': ('required', int),
    'downvotes': ('required', int),
    'replies': ('required', (list, 'items',
        (dict, 'explicit', {
            'comment': ('required', str),
            'utc_datetime': ('required', datetime),
        })
    ))
})

SEQUENCE_RECORD_STORAGE_SCHEMA = (dict, 'explicit', {
    '_id': ('required', str),
    'sequence': ('required', str),
    'metadata': ('required', METADATA_STORAGE_SCHEMA),
    'identifications': ('required', (list, 'items', IDENTIFICATION_STORAGE_SCHEMA))
})

SEQUENCE_RESPOSNE_SCHEMA = (dict, 'explicit', {
    'sequence': ('required', str),
    'metadata': ('required', METADATA_STORAGE_SCHEMA),
    'identifications': ('required', (list, 'items', IDENTIFICATION_STORAGE_SCHEMA))
})

SEARCH_RESPONSE_SCHEMA = (list, 'items', (dict, 'explicit', {
    'hash': ('required', str),
    'metadata': ('required', METADATA_STORAGE_SCHEMA),
    'identifications': ('required', (list, 'items', IDENTIFICATION_STORAGE_SCHEMA))
}))

UPLOAD_REQUEST_SCHEMA = (dict, 'explicit', {
    'fasta': ('required', str),
    'metadata': ('required', (dict, 'explicit', {
        'name': ('required', str),
        'organism': ('required', str),
        'organism_confidence': ('required', Confidence),
        'comment': ('required', str)
    }))
})

COMMENT_REQUEST_SCHEMA = (dict, 'explicit', {
    'hash': ('required', str),
    'identification': ('required', (dict, 'explicit', {
        'organism': ('required', str),
        'organism_confidence': ('required', Confidence),
        'comment': ('required', str)
    }))
})

VOTE_REQUEST_SCHEMA = (dict, 'explicit', {
    'hash': ('required', str),
    'identification': ('required', str),
    'upvote': ('required', (int, 'custom',
                            lambda data, qualname: None if data in {-1, 0, 1}
                                                        else SchemaValueError(f'{qualname}["upvote"] must be -1, 0, or 1'))),
    'downvote': ('required', (int, 'custom',
                              lambda data, qualname: None if data in {-1, 0, 1}
                                                          else SchemaValueError(f'{qualname}["downvote"] must be -1, 0, or 1'))),
})

REPLY_REQUEST_SCHEMA = (dict, 'explicit', {
    'hash': ('required', str),
    'identification': ('required', str),
    'reply': ('required', str)
})
