#!/usr/bin/env python3
from datetime import datetime
import json

from pymongo import MongoClient
from pymongo.server_api import ServerApi
from nog642_util import validate_schema

from Sequence import Sequence
from schemas import SEQUENCE_RECORD_STORAGE_SCHEMA


class GeneDatabase:

    METADATA_KEYS = {'name', 'organism', 'organism_confidence'}

    def __init__(self):
        self.mongo_client = MongoClient('localhost', 6202, server_api=ServerApi('1'))
        self.seq_db = self.mongo_client['sequences']
        self.sequences = self.seq_db['sequences']

    def add_sequence(self, sequence: Sequence, metadata):

        if not isinstance(metadata, dict):
            return False, 'Metadata must be a json object'
        comment = metadata.pop('comment')
        if metadata.keys() != self.METADATA_KEYS:
            return False, f'Metadata keys must be {self.METADATA_KEYS}'
        if not all(isinstance(v, str) for v in metadata.values()):
            return False, f'Metadata values must be strings'
        if not metadata['name']:
            return False, 'name cannot be empty'

        result = self.sequences.find_one({'_id': sequence.sha256_hash})
        if result is not None:
            return False, f'This sequence is already in the database: {sequence.sha256_hash}'
        metadata = {k: metadata[k] for k in self.METADATA_KEYS}
        utc_now = datetime.utcnow()
        metadata['utc_datetime'] = utc_now
        entry = {
            '_id': sequence.sha256_hash,
            'sequence': sequence.sequence,
            'metadata': metadata,
            'identifications': [
                {
                    'author': 'uploader',
                    'identity': metadata['organism'],
                    'confidence': metadata['organism_confidence'],
                    'comment': comment,
                    'utc_datetime': utc_now,
                    'upvotes': 0,
                    'downvotes': 0,
                    'replies': []
                }
            ]
        }
        validate_schema(data=entry, schema=SEQUENCE_RECORD_STORAGE_SCHEMA)
        self.sequences.insert_one(entry)
        return True, None

    def get_sequence(self, seq_hash: str):
        result = self.sequences.find_one({'_id': seq_hash})
        if result is None:
            return None, 'This sequence is not in the database'
        return {
            'sequence': Sequence(result['sequence'], validate=False),
            'metadata': result['metadata'],
            'identifications': result['identifications']
        }, None

    def get_all_sequences(self):
        for record in self.sequences.find():
            yield {
                'hash': record['_id'],
                'metadata': record['metadata'],
                'identifications': record['identifications']
            }

    def add_identification(self, seq_hash, identification):
        # TODO: add locking
        entry = self.sequences.find_one({'_id': seq_hash})
        if entry is None:
            return None, 'Sequence not found'
        for existing_identification_entry in entry['identifications']:
            if existing_identification_entry['identity'] == identification['organism']:
                return None, (f"There is already an identification as {json.dumps(identification['organism'])}. "
                              f'Consider commenting on and upvoting that one instead.')

        utc_now = datetime.utcnow()
        entry['identifications'].append({
            'author': None,
            'identity': identification['organism'],
            'confidence': identification['organism_confidence'],
            'comment': identification['comment'],
            'utc_datetime': utc_now,
            'upvotes': 0,
            'downvotes': 0,
            'replies': []
        })
        # TODO: use update_one?
        validate_schema(data=entry, schema=SEQUENCE_RECORD_STORAGE_SCHEMA)
        self.sequences.replace_one({'_id': seq_hash}, entry)
        return entry, None

    @staticmethod
    def _update_best_identification(entry: dict):
        """
        mutates entry
        """

        # the first entry is the uploader's identification
        best_identification = entry['identifications'][0]

        # require at least 3 votes to override the uploader's identification
        best_votes = max(best_identification['upvotes'] - best_identification['downvotes'], 3)

        for identification in entry['identifications']:
            votes = identification['upvotes'] - identification['downvotes']
            if votes > best_votes:
                best_votes = votes
                best_identification = identification

        entry['metadata']['organism'] = best_identification['identity']

        # TODO: is there a better way to set this, that uses the number of votes?
        entry['metadata']['organism_confidence'] = best_identification['confidence']

    def vote_identification(self, seq_hash: str, identification: str, upvotes: int, downvotes: int):
        # TODO: add locking
        entry = self.sequences.find_one({'_id': seq_hash})
        if entry is None:
            return 'Sequence not found'

        identification_idx = None
        for i, existing_identification_entry in enumerate(entry['identifications']):
            if existing_identification_entry['identity'] == identification:
                identification_idx = i
                break
        if identification_idx is None:
            return f'identification {identification!r} not found'

        entry['identifications'][identification_idx]['upvotes'] += upvotes
        entry['identifications'][identification_idx]['downvotes'] += downvotes

        self._update_best_identification(entry)

        # TODO: use update_one?
        validate_schema(data=entry, schema=SEQUENCE_RECORD_STORAGE_SCHEMA)
        self.sequences.replace_one({'_id': seq_hash}, entry)

    def add_reply(self, seq_hash: str, identification: str, comment: str):
        # TODO: add locking
        entry = self.sequences.find_one({'_id': seq_hash})
        if entry is None:
            return 'Sequence not found'

        identification_idx = None
        for i, existing_identification_entry in enumerate(entry['identifications']):
            if existing_identification_entry['identity'] == identification:
                identification_idx = i
                break
        if identification_idx is None:
            return f'identification {identification!r} not found'

        utc_now = datetime.utcnow()
        entry['identifications'][identification_idx]['replies'].append({
            'comment': comment,
            'utc_datetime': utc_now
        })
        # TODO: use update_one?
        validate_schema(data=entry, schema=SEQUENCE_RECORD_STORAGE_SCHEMA)
        self.sequences.replace_one({'_id': seq_hash}, entry)


def test_main():
    db = GeneDatabase()
    # success, error_message = db.add_sequence(Sequence('atcg'), None)
    # if not success:
    #     print(error_message)
    # print(list(db.sequences.find()))
    print(db.get_sequence('414322309db5c06d090a2e922ccc3e00708c993b9b96405de127b7fd8da2dd21'))


if __name__ == '__main__':
    test_main()
