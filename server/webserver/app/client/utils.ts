export const apiRequest = async <T>({method, path, queryParams=undefined, body=undefined, onError,
                                     defaultErrorMessage="There was an unexpected error."}: {
    method: string,
    path: string,
    queryParams?: URLSearchParams,
    body?: any
    onError: (error: string) => any,
    defaultErrorMessage?: string
}): Promise<T | undefined | null> => {
    const fetchParams : {
        method: string,
        body?: string,
        headers?: {"Content-type": string}
    } = {
        method: method
    };
    let resource = path;
    if (queryParams !== undefined) {
        resource += "?" + queryParams.toString();
    }
    if (body !== undefined) {
        fetchParams.body = JSON.stringify(body);
        fetchParams.headers = {
            "Content-type": "application/json; charset=UTF-8"
        };
    }
    const response = await fetch(resource, fetchParams);
    const jsonBody = response.headers.get("Content-Type")?.includes("application/json") ? (
        await response.json()
    ) : undefined;
    if (!response.ok || !jsonBody?.success) {
        if (jsonBody?.error) {
            onError(jsonBody.error);
        } else {
            onError(defaultErrorMessage);
        }
        return undefined;
    }
    return jsonBody?.data;
};
