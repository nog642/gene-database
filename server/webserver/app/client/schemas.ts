export enum Confidence {
    low = "low",
    medium = "medium",
    high = "high",
    very_high = "very_high",
    imported_from_database =  "imported_from_database"
}

export interface SequenceMetadata {
    name: string;
    organism: string;
    organism_confidence: Confidence;
    utc_datetime: string;
}

export interface ReplyEntry {
    comment: string;
    utc_datetime: string;
}

export interface SequenceIdentification {
    author: string | null;
    identity: string;
    confidence: Confidence;
    comment: string;
    utc_datetime: string;
    upvotes: number;
    downvotes: number;
    replies: ReplyEntry[];
}

export interface SequenceResponseData {
    sequence: string;
    metadata: SequenceMetadata;
    identifications: SequenceIdentification[];
}

export interface SearchResultsEntry {
    hash: string;
    metadata: SequenceMetadata;
    identifications: SequenceIdentification[];
}

export interface UploadRequestData {
    fasta: string;
    metadata: {
        name: string,
        organism: string,
        organism_confidence: Confidence,
        comment: string
    }
}

export interface CommentRequestData {
    hash: string;
    identification: {
        organism: string,
        organism_confidence: string,
        comment: string
    }
}

export interface VoteRequestData {
    hash: string;
    identification: string,
    upvote: -1 | 0 | 1,
    downvote: -1 | 0 | 1
}

export interface ReplyRequestData {
    hash: string;
    identification: string;
    reply: string;
}
