import type { SequenceResponseData, SequenceIdentification, CommentRequestData,
              VoteRequestData, ReplyRequestData } from "schemas.js";
import { apiRequest } from "./utils.js";


const getIdentificationLocalStorage = (hash: string, identification: string) => {
    let seqObjStr = window.localStorage.getItem(hash);
    let seqObj;
    if (seqObjStr === null) {
        seqObj = {
            [identification]: {}
        };
        seqObjStr = JSON.stringify(seqObj);
        window.localStorage.setItem(hash, seqObjStr);
    } else {
        seqObj = JSON.parse(seqObjStr);
        if (!seqObj.hasOwnProperty(identification)) {
            seqObj[identification] = {}
            seqObjStr = JSON.stringify(seqObj);
            window.localStorage.setItem(hash, seqObjStr);
        }
    }
    return seqObj[identification];
}

const setIdentificationLocalStorage = (hash: string, identification: string, value: any) => {
    let seqObjStr = window.localStorage.getItem(hash);
    let seqObj;
    if (seqObjStr === null) {
        seqObj = {
            [identification]: value
        };
    } else {
        seqObj = JSON.parse(seqObjStr);
        seqObj[identification] = value;
    }
    seqObjStr = JSON.stringify(seqObj);
    window.localStorage.setItem(hash, seqObjStr);
}

$(async () => {
    const seqHash = (() => {
        let path = window.location.pathname.substring(1);
        const startSlash = path.indexOf('/');
        path = path.substring(startSlash + 1);
        const endSlash = path.indexOf('/');
        if (endSlash > -1) {
            path = path.substring(0, endSlash);
        }
        return path;
    })();
    const seq_data = await apiRequest<SequenceResponseData>({
        method: "GET",
        path: "/api/get_seq_data",
        queryParams: new URLSearchParams({
            seq: seqHash
        }),
        onError: (error: string) => console.error(`Error in /api/get_seq_data API call: ${error}`)
    });
    if (!seq_data) {
        // there was an error
        return;
    }
    document.getElementById("seqnamefield")!.innerText = seq_data.metadata.name;
    document.getElementById("sequencefield")!.innerText = seq_data.sequence;
    document.getElementById("organismfield")!.innerText = seq_data.metadata.organism;
    document.getElementById("confidencefield")!.innerText = seq_data.metadata.organism_confidence;
    const identificationsList = document.getElementById("identificationsList")!;

    const addField = (addToDiv: HTMLDivElement, title: string, val: string) => {
        const pElem = document.createElement("p");
        const bElem = document.createElement("b");
        bElem.innerText = `${title}:`;
        pElem.appendChild(bElem);
        pElem.appendChild(document.createTextNode(` ${val}`));
        addToDiv.appendChild(pElem);
    };
    const addIdentification = (identification: SequenceIdentification, idNum: number) => {
        const idDiv = document.createElement("div");
        idDiv.className = "identificationItem";
        idDiv.id = `id${idNum}`;
        if (identification.author !== null) {
            let author;
            if (identification.author === "uploader") {
                author = "Uploader";
            } else {
                author = identification.author;
            }
            addField(idDiv, "Author", author);
        }
        addField(idDiv, "Identity", identification.identity);
        addField(idDiv, "Confidence", identification.confidence);
        if (identification.comment !== null) {
            addField(idDiv, "Comment", identification.comment);
        }
        addField(idDiv, "Datetime", identification.utc_datetime);

        // add upvote and downvote buttons
        const context = getIdentificationLocalStorage(seqHash, identification.identity);
        const vote = async (upvote: boolean, downvote: boolean, unupvote: boolean, undownvote: boolean) => {
            if (downvote && context.upvoted) {
                unupvote = true;
            }
            if (upvote && context.downvoted) {
                undownvote = true;
            }
            let upvotes = 0;
            let downvotes = 0;
            if (upvote) {
                console.log("upvote");
                ++upvotes;
            }
            if (downvote) {
                console.log("downvote");
                ++downvotes;
            }
            if (unupvote) {
                console.log("unpvote");
                --upvotes;
            }
            if (undownvote) {
                console.log("undownvote");
                --downvotes;
            }
            const result = await apiRequest<null>({
                method: "POST",
                path: "/api/vote",
                body: {
                    hash: seqHash,
                    identification: identification.identity,
                    upvote: upvotes as -1 | 0 | 1,
                    downvote: downvotes as -1 | 0 | 1
                } satisfies VoteRequestData,
                onError: (error: string) => console.error(`Error in /api/vote API call: ${error}`)
            });
            if (result !== undefined) {
                setIdentificationLocalStorage(seqHash, identification.identity, {
                    upvoted: upvote,
                    downvoted: downvote
                });

                // reload page, jump to this identification
                window.location.hash = idDiv.id;
                window.location.reload();
            }
        };
        const voteP = document.createElement("p");
        voteP.style.whiteSpace = "pre";
        const upvoteButton = document.createElement("button");
        upvoteButton.innerText = "Upvote";
        if (context.upvoted) {
            upvoteButton.style.backgroundColor = "#99ff99";
            upvoteButton.addEventListener("click", event => vote(false, false, true, false));
        } else {
            upvoteButton.addEventListener("click", event => vote(true, false, false, false));
        }
        const downvoteButton = document.createElement("button");
        downvoteButton.innerText = "Downvote";
        if (context.downvoted) {
            downvoteButton.style.backgroundColor = "#99ff99";
            downvoteButton.addEventListener("click", event => vote(false, false, false, true));
        } else {
            downvoteButton.addEventListener("click", event => vote(false, true, false, false));
        }
        voteP.appendChild(upvoteButton);
        voteP.appendChild(document.createTextNode(` (${identification.upvotes})    `));
        voteP.appendChild(downvoteButton);
        voteP.appendChild(document.createTextNode(` (${identification.downvotes})    `));
        idDiv.appendChild(voteP);

        // add reply button
        const replyButton = document.createElement("button");
        replyButton.innerText = "Comment";
        const replyForm = document.createElement("form");
        const replyBox = document.createElement("input");
        replyBox.style.width = "90%";
        const submitReply = document.createElement("button");
        submitReply.innerText = "Submit";
        submitReply.type = "submit";
        replyForm.appendChild(replyBox);
        replyForm.appendChild(document.createElement("br"));
        replyForm.appendChild(submitReply);
        replyForm.style.display = "none";
        replyButton.addEventListener("click", event => {
            if (replyForm.style.display == "none") {
                replyForm.style.display = "block";
            } else {
                replyForm.style.display = "none";
            }
        });
        replyForm.addEventListener("submit", async event => {
            event.preventDefault();
            const result = await apiRequest({
                method: "POST",
                path: "/api/reply",
                body: {
                    hash: seqHash,
                    identification: identification.identity,
                    reply: replyBox.value
                } satisfies ReplyRequestData,
                onError: (error: string) => console.error(`Error in /api/reply API call: ${error}`)
            });
            if (result !== undefined) {
                // reload page, jump to this identification
                window.location.hash = idDiv.id;
                window.location.reload();
            }

        });
        idDiv.appendChild(replyForm);
        voteP.appendChild(replyButton);

        // add replies
        for (const reply of identification.replies) {
            const replyDiv = document.createElement("div");
            replyDiv.className = "identificationItem";
            addField(replyDiv, "Comment", reply.comment);
            addField(replyDiv, "Datetime", reply.utc_datetime);
            idDiv.appendChild(replyDiv);
        }

        identificationsList.appendChild(idDiv);
    };

    console.log(seq_data.identifications);
    let i = 0;
    for (const identification of seq_data.identifications) {
        addIdentification(identification, i);
        ++i;
    }

    document.getElementById("addidform")!
            .addEventListener("submit", async event => {
        event.preventDefault();
        const seq_organism = (document.getElementById("seqidentityinput") as HTMLInputElement).value;
        const organism_confidence = (document.getElementById("confidenceinput") as HTMLSelectElement).value;
        const comment = (document.getElementById("commentinput") as HTMLInputElement).value;
        const reqBody: CommentRequestData = {
            hash: seqHash,
            identification: {
                organism: seq_organism,
                organism_confidence: organism_confidence,
                comment: comment
            }
        };
        const result = await apiRequest<null>({
            method: "POST",
            path: "/api/comment",
            body: reqBody,
            onError: (error: string) => {
                document.getElementById("errorfield")!.innerText = error;
            }
        });
        if (result !== undefined) {
            window.location.reload();
        }
    });

    if (location.hash) {
        // jump to dynamically created element
        window.location.replace(location.hash);
    }
});
