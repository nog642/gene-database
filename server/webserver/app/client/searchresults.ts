import type { SearchResultsEntry } from "schemas.js";
import { apiRequest } from "./utils.js";

$(async () => {
    let path = window.location.pathname.substring(1);
    const startSlash = path.indexOf('/');
    path = path.substring(startSlash + 1);
    const endSlash = path.indexOf('/');
    if (endSlash > -1) {
        path = path.substring(0, endSlash);
    }
    const search_results = await apiRequest<SearchResultsEntry[]>({
        method: "GET",
        path: "/api/search",
        onError: (error: string) => console.error(`Error in /api/search API call: ${error}`)
    });
    // const response = await fetch("/search", {
    //     method: "POST"
    // });
    // const search_results = await response.json();
    if (search_results === undefined || search_results === null) {
        // error
        return;
    }
    const table = document.getElementById("resultstable")!;
    for (const result of search_results) {
        const row = document.createElement("tr");
        const col1 = document.createElement("td");
        const col2 = document.createElement("td");
        const link = document.createElement("a");
        link.innerText = result.metadata.name;
        link.href = "/seq/" + result.hash;
        col1.appendChild(link);
        col1.classList.add("sequence-field")
        col2.innerText = result.metadata.organism;
        row.appendChild(col1);
        row.appendChild(col2);
        table.appendChild(row);
    }
});
