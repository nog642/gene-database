$(() => {
    document.getElementById("topbarsearchsubmit")!
            .addEventListener("click", event => {
        const search_query = (document.getElementById("topbarsearchbar") as HTMLInputElement).value;
        const params = new URLSearchParams({
          query: search_query
        });
        window.location.replace("/search?" + params.toString());
    });
});
