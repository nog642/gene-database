import type { Confidence, UploadRequestData } from "schemas.js";
import { apiRequest } from "./utils.js";

$(() => {
    document.getElementById("uploadform")!
            .addEventListener("submit", async event => {
        event.preventDefault();
        const seq_name = (document.getElementById("seqnameinput") as HTMLInputElement).value;
        const sequence = (document.getElementById("sequenceinput") as HTMLTextAreaElement).value;
        const seq_organism = (document.getElementById("seqidentityinput") as HTMLInputElement).value;
        const organism_confidence = (document.getElementById("confidenceinput") as HTMLSelectElement).value;
        const comment = (document.getElementById("commentinput") as HTMLInputElement).value;
        const reqBody: UploadRequestData = {
            fasta: sequence,
            metadata: {
                name: seq_name,
                organism: seq_organism,
                organism_confidence: organism_confidence as Confidence,  // TODO: validate this
                comment: comment
            }
        }
        const uploadedSeqId = await apiRequest<string>({
            method: "POST",
            path: "/api/upload",
            body: reqBody,
            onError: (error: string) => {
                document.getElementById("errorfield")!.innerText = error;
            }
        });
        if (uploadedSeqId) {
            window.location.replace("/seq/" + uploadedSeqId);
        }
    });
});
